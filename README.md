# forget-me-NOTe #
####Introduction
**forget-me-NOTe** is a project that is essentially comprised of two integral parts - the library and the client app. They go in bundle for installation. Library is a standalone package called *forgetmenote* that resides inside *forgetmenote_app* package and provides key functionality for tasktracking. The client app here is accessible via console script interface and fully relies on the API and types exposed for public use by *forgetmenote* library. The client code can be spotted in *forgetmenote_client* package inside *forgetmenote-app* package.

Generally speaking, tasktrackers are apps developed for keeping your tasks, notes or plans all at one place with some extra facilities like differentiating tasks by status, issuing notifications, reminders and so on. Popular examples are **TickTick**, **Wunderlist**, **Trello**.

This project's intention is to create an app that would encompass all the key interface of the forementioned systems, implement some new ideas and see what it entails.

####Installation
First, download the source. 
```bash
cd <where you want the project to be>
git clone <this_repo_url>
cd forget-me-note
```
Then, install the app to your environment
```bash
python setup.py install
```
There you go. Now it's all set up. Just run the necessary commands and enjoy. Check out the following section to learn more about usage details.
```bash
forgetmenote ...
```

####Usage
Available commands:
```bash
# user
forgetmenote login
forgetmenote who
 
 # task commands
forgetmenote task create
forgetmenote task delete
forgetmenote task update
forgetmenote task get
forgetmenote task copy
forgetmenote task complete
forgetmenote task resume
forgetmenote task cancel
forgetmenote task repeat set
forgetmenote task repeat unset
forgetmenote task find

# subtask commands
forgetmenote subtask create
forgetmenote subtask update
forgetmenote subtask copy
forgetmenote subtask find

# group commands 
forgetmenote group create
forgetmenote group delete
forgetmenote group update
forgetmenote group get
forgetmenote group find

# reminder commands
forgetmenote reminder set
forgetmenote reminder dismiss
forgetmenote reminder find

# relation commands
forgetmenote relation create
forgetmenote relation delete
forgetmenote relation find

# permission commands
forgetmenote permission grant
forgetmenote permission withdraw
forgetmenote permission find
```
For more details on the argument lists checkout *\-\-help* for each command.

####Tests
The root package of the whole project is *forgetmenote_app*.

It's been already mentioned that the project consists of two parts - library and client app. Well, that's not entirely true. In fact, inside *forgetmenote_app* package you can find one more package *tests* that contains test suites for the library code. To run tests type the following in the command line while being in the root folder of the cloned repository
```bash
python setup.py test
```
You can examine *tests* package for more information on how the lib behaves in some particular cases.

####Library
To learn more about library facilities checkout help(lib) since the library supports detailed fine-grained documentation.

There are a couple of options to do that. The recommended one is to open your python shell.
```python
from forgetmenote_app import forgetmenote
help(forgetmenote)
```


*Author - Angelina Belyasova, 2018*

*Email - angelinabelyasova@gmail.com*