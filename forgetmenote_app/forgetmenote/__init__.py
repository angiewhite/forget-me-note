"""Library that encompasses logic for task management and is to be used for developing tasktracking clients.

Library contains the following packages:
models - package that defines all basic models of the library related to the domain
models_services - package that contains core functionality of the library (the heart of it)
storage_services - package that contains storage manipulation logic and defines storage interface

However, it is not recommended to include any package other than the root one since all public types are already accessible through the library package name after import.

Available for public use types and methods:

models:
ModelObjectNotFoundError, AccessDeniedError (exceptions module) - custom exceptions raised in library
Group (group module) - type describing group model
Permission, AccessType (permission module) - types related to permission model
Relation, RelationType (relation module) - types related to relation model
Reminder, ReminderStatus (reminder module) - types representing reminder model
Task, Priority, Status (task module) - task-related types

models_services:
ServicesManager (services_manager module) - use it get access to all the services provided by the library
SortKeyGetter (helpers module) - helper class for specify sort key

storage_services:
Storage (storage) - type that has to be instantiated and, by doing so, then initialize ServicesManager so that it can perform its services

log module:
get_logger - use it to get logger the library uses. When no configuration is provided - nothing is logged.
"""
LIB_NAME = __name__


import logging
logger = logging.getLogger(LIB_NAME)
logger.addHandler(logging.NullHandler())

from .models import (ModelObjectNotFoundError, AccessDeniedError,
                     Group, Permission, AccessType, Relation,
                     RelationType, Reminder, ReminderStatus,
                     Task, Priority, Status)
from .models_services import ServicesManager, SortKeyGetter
from .storage_services.storage import Storage
from .log import get_logger

__all__ = ['ModelObjectNotFoundError', 'AccessDeniedError',
           'Group', 'Permission', 'AccessType', 'Relation',
           'RelationType', 'Reminder', 'ReminderStatus',
           'Task', 'Priority', 'Status',
           'ServicesManager', 'SortKeyGetter',
           'Storage', 'get_logger']