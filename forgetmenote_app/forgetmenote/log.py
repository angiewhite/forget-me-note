import logging
import functools
import forgetmenote.models as models
from forgetmenote import LIB_NAME


def log(logger_name, level=logging.INFO):
    def log_decorator(func):

        @functools.wraps(func)
        def logged_func(*args, **kwargs):
            logger = logging.getLogger(logger_name)
            logger.log(level,
                       'Trying to enter function {} with {}, {}'.format(func.__name__,
                                                                        args, kwargs))
            try:
                result = func(*args, **kwargs)
                logger.log(level,
                           'Successfully exited function {} '
                           'and returned {}'.format(func.__name__, result))
                return result
            except Exception as e:
                logger.warning('Exception {} with {}'.format(type(e),
                                                             e.args))
                raise
        return logged_func
    return log_decorator


def get_logger():
    """Use it to get lib's logger and configure it
    :return: logging.Logger - lib's unique logger
    """
    return logging.getLogger(LIB_NAME)
