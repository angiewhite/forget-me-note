"""Module containing custom exceptions of the lib."""


class ModelObjectNotFoundError(Exception):
    """
    Exception is thrown when the object of type with specified id was not found.
    It is used in cases when some operation is to be performed on the object that does not exist.

    Instance attributes:
    object_id: int
    model_type: int
    """

    def __init__(self, model_type, object_id):
        """
        Extends __init__ of Exception in order to form a more
        informative message (using type and id) about the failed query parameters.

        :param model_type: type - the type of object to be found
        :param object_id: int - the id of object to be found
        """
        self.object_id = object_id
        self.model_type = model_type
        self.message = ('{} with such id {} '
                        'was not found.'.format(model_type.__name__, object_id))
        super(ModelObjectNotFoundError, self).__init__(self.message)


class AccessDeniedError(Exception):
    """
    Exception is thrown when a user doesn't have permission to request an operation.
    """
    pass
