"""Module containing group model class."""


class Group:
    """Class that aggregates attributes that pertain to a group instance.
    Groups are object that classify tasks.

    Instance attributes:
    id: int - id of the group
    creator_id: int - user who created it
    title: str - title of the group
    """

    def __init__(self, obj_id, creator_id, title):
        self.id = obj_id
        self.creator_id = creator_id
        self.title = title

    def __eq__(self, other):
        return (self.id == other.id and self.creator_id == other.creator_id
                and self.title == other.title)

    def __str__(self):
        return 'Group: id={}, creator_id={}, title={}'.format(self.id,
                                                              self.creator_id,
                                                              self.title)
