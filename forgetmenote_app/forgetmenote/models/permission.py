"""Module containing types for permission logic support."""


import enum


class AccessType(enum.Enum):
    """
    Enum (extends enum.Enum) to describe the kind of permission a user has.
    There is a special kind of access - creator - that is superior to all other.

    Options:
    MANAGER (2) - the one who can cancel and resume tasks and also grant executor and viewer type permissions
    EXECUTOR (1) - the one who can complete tasks
    VIEWER (0) - the one who can only view and copy task he has access to
    """

    MANAGER = 2
    EXECUTOR = 1
    VIEWER = 0


class Permission:
    """Class that describes a permission instance.
    Permissions are abstract objects that endow a user with a certain kind of access to a task.

    Instance attributes:
    id: int - id of permission object
    task_id: int - id of the task to which access is provided
    user_id: int - id of the user to who access id provided
    access_type: AccessType (enum.Enum) - type of access provided
    """

    def __init__(self, obj_id, task_id, user_id, access_type):
        self.id = obj_id
        self.task_id = task_id
        self.user_id = user_id
        self.access_type = access_type

    def __str__(self):
        return ('Permission: id={}, task_id={}, '
                'user_id={}, access_type={}'.format(self.id,
                                                    self.task_id,
                                                    self.user_id,
                                                    self.access_type))