"""Module containing relation logic types."""


import enum


class RelationType(enum.Enum):
    """Enum (extends enum.Enum) that indicates the type of relation between two tasks.

    Options:
    FOLLOWS (0) - a task needs to be complete after the one it's related to
    PRECEDES (1) - a task needs to be complete before the one it's related to
    SIMULTANEOUS (2) - tasks need to be performed at the same time
    BLOCKING (3) - tasks can't be performed at the same time
    INFORMATIVE (4) - a task depends on the result or knowledge gained by doing the other task
    """
    FOLLOWS = 0
    PRECEDES = 1
    SIMULTANEOUS = 2
    BLOCKING = 3
    INFORMATIVE = 4


class Relation:
    """Class that describes relation model instances.
    Relations are objects that link two tasks together in a certain way defined by relation_type.

    Instance attributes:
    id: int - id of the relation object
    creator_id: int - id of relation creator
    from_task_id: int - id of the task where relation originates
    to_task_id: int - id of the task where relation ends
    relation_type: RelationType (enum.Enum) - type of relation
    """

    def __init__(self, obj_id, creator_id, from_task_id, to_task_id, relation_type):
        self.id = obj_id
        self.creator_id = creator_id
        self.from_task_id = from_task_id
        self.to_task_id = to_task_id
        self.relation_type = relation_type

    def __str__(self):
        return ('Relation: id={}, creator_id={}, '
                'from_task_id={}, to_task_id={}, '
                'relation_type={}'.format(self.id,
                                          self.creator_id,
                                          self.from_task_id,
                                          self.to_task_id,
                                          self.relation_type))