"""Module containing reminder logic types."""


import enum


class ReminderStatus(enum.Enum):
    """Enum (extends enum.Enum) to indicate a reminder status.

    Options:
    PENDING (0) - waiting for the time to perform its mission
    WORKED (1) - already performed its mission
    CANCELED (2) - haven't performed yet and won't do on time
    """

    PENDING = 0
    WORKED = 1
    CANCELED = 2


class Reminder:
    """Class that aggregates attributes pertaining to a reminder instance.
    Reminders are objects that contain information about reminding action for task.

    Instance attributes:
    id: int - id of the reminder object
    task_id: int - id of the task the reminder ib bound to
    time: datetime.datetime - time to remind (perform mission)
    status: ReminderStatus (enum.Enum) - current status of the reminder
    """

    def __init__(self, obj_id, task_id, time, status):
        self.id = obj_id
        self.task_id = task_id
        self.time = time
        self.status = status

    def __str__(self):
        return ('Reminder: id={}, task_id={}, '
                'time={}, status={}'.format(self.id,
                                            self.task_id,
                                            self.time,
                                            self.status))
