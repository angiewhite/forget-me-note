"""Module containing types related to task model."""


import enum


class Priority(enum.Enum):
    """
    Enum (extends enum.Enum) to indicate the priority of a task.

    Options:
    NONE (3) - a task doesn't have any priority
    LOW (2) - a task with low priority
    MEDIUM (1) - a task with medium priority
    HIGH (0) - a task with high priority
    """

    NONE = 3
    LOW = 2
    MEDIUM = 1
    HIGH = 0


class Status(enum.Enum):
    """
    Enum (extends enum.Enum) to indicate current status of a task.

    Options:
    PENDING (0) - is waiting to be done
    DONE (1) - is done by a user
    FAILED (2) - was failed to be done till deadline
    CANCELED (3) - was put off wait and will not get FAILED on time of deadline
    """

    PENDING = 0
    DONE = 1
    FAILED = 2
    CANCELED = 3


class Task:
    """Task is an object that can be assigned to a user and is to be done by him.

    Instance attributes:
    id: int - id of the task
    creator_id: int - id of the creator (with superior rights)
    group_id: int - id of the group a task belongs to
    deadline: datetime.datetime or None - ultimate time for task to be done
    creation_time: datetime.datetime - time when task was created
    last_updated_time: datetime.datetime or None - the last time a task was modified
    title: str - the title of the task
    description: str - the description of the task
    priority: Priority (enum.Enum) - priority of the task
    status: Status (enum.Enum) - status of the task
    repeat: bool - is it repeatable
    repeat_period: datetime.datetime or None - time shift at repeat handling

    Two tasks objects are equal if all their attributes return True under '==' comparison.
    """

    def __init__(self, obj_id, creator_id, parent_task_id, group_id,
                 title, description, deadline, priority, status,
                 creation_time, last_updated_time):
        self.id = obj_id
        self.creator_id = creator_id
        self.parent_task_id = parent_task_id
        self.group_id = group_id
        self.deadline = deadline
        self.creation_time = creation_time
        self.last_updated_time = last_updated_time
        self.title = title
        self.description = description
        self.priority = priority
        self.status = status
        self.repeat = False
        self.repeat_period = None

    def __eq__(self, other):
        return (self.id == other.id
                and self.creation_time == other.creation_time
                and self.last_updated_time == other.last_updated_time
                and self.status == other.status
                and self.is_copy(other))

    def is_copy(self, other):
        def repeat_period_equal():
            if self.repeat_period == other.repeat_period:
                return True

            if not self.repeat_period or not other.repeat_period:
                return False

            if (self.repeat_period._freq == other.repeat_period._freq
                    and self.repeat_period._count == other.repeat_period._count
                    and self.repeat_period._interval == other.repeat_period._interval
                    and self.repeat_period._byweekday == other.repeat_period._byweekday):
                return True

            return False

        return (self.creator_id == other.creator_id
                and self.group_id == other.group_id
                and self.deadline == other.deadline
                and self.title == other.title
                and self.description == other.description
                and self.priority == other.priority
                and self.repeat == other.repeat
                and repeat_period_equal())

    def __str__(self):
        return ('Task: id={}, creator_id={}, group_id={}, parent_task_id={}, '
                'deadline={}, title={}, description={}, priority={}, status={}, '
                'repeat={}, repeat_period={}, creation_time={}, '
                'last_updated_time={}'.format(self.id, self.creator_id, self.group_id,
                                              self.parent_task_id, self.deadline,
                                              self.title, self.description,
                                              self.priority.name, self.status.name,
                                              self.repeat, repr(self.repeat_period),
                                              self.creation_time,
                                              self.last_updated_time))