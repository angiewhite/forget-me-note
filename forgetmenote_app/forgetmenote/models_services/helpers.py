import functools
import datetime
import forgetmenote.models


class SortKeyGetter:

    @staticmethod
    def DEFAULT(o):
        return o.title

    @staticmethod
    def PRIORITY(o):
        return o.priority.value

    @staticmethod
    def DEADLINE(o):
        return o.deadline if o.deadline else datetime.datetime.max

    @staticmethod
    def STATUS(o):
        return o.status.value


def change_last_updated_time(func):

    @functools.wraps(func)
    def replace_func(self, current_user_id, task_id, *args, **kwargs):
        result = func(self, current_user_id, task_id, *args, **kwargs)
        task = self.storage.update(forgetmenote.models.Task, task_id,
                                   last_updated_time=datetime.datetime.now())
        if result and task.id == result.id:
            result = task
        return result
    return replace_func


def target_argument_getter(index, arg_name):
    def getter(self, args, kwargs):
        try:
            return args[index]
        except IndexError:
            return kwargs[arg_name]
    return getter


