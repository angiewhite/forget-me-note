import logging
import datetime
import forgetmenote.str_constants as constants
from forgetmenote import models
from forgetmenote.log import log
from forgetmenote.models_services.permission_services import (
    creator_access, manager_access,
    executor_access, viewer_access,
    group_creator_access
)
from forgetmenote.models_services.helpers import (change_last_updated_time, SortKeyGetter,
                                                  target_argument_getter)


def services_manager_log(level=logging.INFO):
    return log(__name__, level=level)


class ServicesManager:
    """Class that provides library's API for managing tasks and other models' objects.

    Public methods:
    - tasks
        create_task - use to create task object
        create_subtask - use to create a subtask of a task
        get_subtasks - use to get task's subtasks
        delete_task - use to delete a task (whether subtask or not)
        update_task_by_id - use to update a task by providing it's id
        update_task - use to update a task by providing it's modified object
        get_tasks - use to get tasks (with filter facility)
        get_task_by_id - use to get task by id (ModelsObjectNotFoundError is raised if a task is absent in self.storage)
        complete_task - use to mark task and all its subtasks as DONE
        cancel_task - use to mark task and all its subtasks as CANCELLED
        resume_task - use to undo task cancelling for task and all its sutasks
        set_repeat - use to set repeat on task
        unset_repeat - use to remove repeat on task
        copy_and_shift_task - use to make a copy of task and replace old deadline
        copy_and_shift_subtask - user to make a copy of and shift subtask
    - reminders
        set_reminder - use to set reminder on task
        dismiss_reminder - use to dismiss reminder (delete it)
        get_reminders - use to get all reminders for a task
    - permissions
        grant_permission - use to grant user with a permission to a task
        withdraw_permission - use to withdraw a certain kind of permission to task from user
        withdraw_all_permissions - use to withdraw all kinds of permissions from the user
        get_permissions - use to get permissions on a task with possible constraints
    - relations
        relate - use to relate to tasks
        delete relation - use to delete certain relation
        get_relations - use to get all relation created by the current user with possible constraints
    - groups
        create_group - use to create group object
        delete_group - use to delete a certain group object
        update_group - use to update a certain group object
        get_groups - use to get groups
        get_group_by_id - use to get group by id or get cursed with ModelsObjectNotFoundError
        get_group_tasks - use to get tasks from the group
    get_overdue_objects - use to get failed tasks, new ones created and worked reminders

    All the manipulations are performed over objects from self.storage.
    """

    @services_manager_log()
    def __init__(self, storage):
        """To initialize provide Storage object or at least object with same interface
        """
        self.storage = storage

    @services_manager_log()
    @group_creator_access()
    def create_task(self, current_user_id, group_id, title, description="",
                    deadline=None, priority=models.Priority.NONE):
        """
        :param current_user_id: int - creator of the task
        :param group_id: int - id of the group where task resides
        :param title: str - name of the task
        :param description: str - more elaborate info on the task
        :param deadline: datetime or None - the ultimate time to perform task
        :param priority: Priority - level of importance
        :return: Task - new task

        Exceptions raised:
        AccessDeniedError - if current user created a task not in his own group
        """

        return self.storage.create(request_type=models.Task, creator_id=current_user_id,
                                   parent_task_id=None, group_id=group_id,
                                   title=title, description=description,
                                   deadline=deadline, priority=priority,
                                   status=models.Status.PENDING,
                                   creation_time=datetime.datetime.now(),
                                   last_updated_time=None)

    @services_manager_log()
    @creator_access(task_argument_getter=
                    target_argument_getter(index=0, arg_name='parent_task_id'))
    def create_subtask(self, current_user_id, parent_task_id, title, description=""):
        """
        :param current_user_id: int - creator of the subtask
        :param parent_task_id: int - id of parent task
        :param title: str - name of the subtask
        :param description: str - more elaborate info on the subtask
        :return: Task - created subtask

        Exceptions raised:
        ModelsObjectNotFoundError - if parent task is not found
        """

        parent_task = self.get_task_by_id(current_user_id, task_id=parent_task_id)

        subtask = self.storage.create(request_type=models.Task,
                                      creator_id=current_user_id,
                                      parent_task_id=parent_task_id,
                                      group_id=parent_task.group_id,
                                      title=title, description=description,
                                      deadline=parent_task.deadline,
                                      priority=parent_task.priority,
                                      status=parent_task.status,
                                      creation_time=datetime.datetime.now(),
                                      last_updated_time=None)

        if parent_task.repeat:
            subtask = self.storage.update(request_type=models.Task, obj_id=subtask.id,
                                          repeat=True, repeat_period=parent_task.repeat_period)
        return subtask

    @services_manager_log()
    @viewer_access()
    def get_subtasks(self, current_user_id, task_id):
        """
        :param current_user_id: int - id of current user
        :param task_id: int - id of task to get subtasks of
        :return: list of Task objects - child tasks of the task with task_id

        Exceptions raised:
        AccessDeniedError - raises when current user doesn't have view access to a task
        """

        return self.get_tasks(current_user_id, parent_task_id=task_id)

    @services_manager_log()
    @creator_access()
    def delete_task(self, current_user_id, task_id):
        """
        :param current_user_id: int - id of current user
        :param task_id: int - id of task to delete
        :return: NoneType

        Exceptions raised:
        ModelsObjectNotFoundError - if task with task_id doesn't exist
        AccessDeniedError - if current user is not creator of the task to delete

        Side effects:
        - removes all subtasks
        - removes all reminders
        - removes all permissions
        """

        # delete all subtasks
        for task in self.get_subtasks(current_user_id, task_id):
            self.delete_task(current_user_id, task.id)

        # cleanup before deletion includes:
        #
        # - deleting reminders
        for reminder in self.get_reminders(current_user_id, task_id):
            self.dismiss_reminder(current_user_id, reminder.id)

        # - deleting permissions
        for permission in self.get_permissions(current_user_id, task_id):
            self.storage.delete(request_type=models.Permission, obj_id=permission.id)

        # - deleting relations
        relations = self.get_relations(current_user_id, from_task_id=task_id)
        relations.extend(self.get_relations(current_user_id, to_task_id=task_id))
        for relation in relations:
            self.delete_relation(current_user_id, relation.id)

        # deletion itself
        self.storage.delete(models.Task, task_id)

    @services_manager_log()
    @creator_access()
    @change_last_updated_time
    def update_task_by_id(self, current_user_id, task_id, group_id=None,
                          set_deadline_none=False, deadline=None,
                          title=None, description=None, priority=None):
        """

        :param current_user_id: int - id of current user
        :param task_id: int - id of task to update
        :param group_id: int - id of group where to move task
        :param set_deadline_none: boolean - set it to True to none deadline
        :param deadline: datetime - new deadline
        :param title: str - new title
        :param description: str - new description
        :param priority: Priority - new priority
        :return: Task - updated task

        Exceptions raised:
        PermissionError - when task_id is id of a subtask or when trying to update failed or done task
        ModelsObjectNotFound - when task is not found
        AccessDeniedError - if current user is not creator of the task to update

        Side effects:
        - if any field aside from title or description is updated - update subtasks
        - if deadline is set to None - repeat period set to None
        """

        task = self.get_task_by_id(current_user_id, task_id)

        if task.parent_task_id is not None:
            raise PermissionError('You cannot update subtask using this method.')

        if (task.status is models.Status.DONE
                or task.status is models.Status.FAILED):
            raise PermissionError('You cannot update a final-state task.')

        update_subtasks = False
        kwargs = {}
        if group_id:
            kwargs[constants.GROUP_ID] = group_id
            update_subtasks = True
        if deadline or set_deadline_none:
            if set_deadline_none:
                deadline = None
            kwargs[constants.DEADLINE] = deadline
            update_subtasks = True
        if title:
            kwargs[constants.TITLE] = title
        if description is not None:
            kwargs[constants.DESCRIPTION] = description
        if priority:
            kwargs[constants.PRIORITY] = priority
            update_subtasks = True

        updated_task = self.storage.update(models.Task, task_id, **kwargs)

        if not updated_task.deadline and updated_task.repeat_period:
            self.set_repeat(current_user_id, task_id, None)

        if not update_subtasks:
            return updated_task

        for task in self.get_subtasks(current_user_id, task_id=updated_task.id):
            self.update_subtask(current_user_id, task.id)

        return updated_task

    @services_manager_log()
    @creator_access(get_task_id=lambda arg: arg.id,
                    task_argument_getter=
                    target_argument_getter(index=0, arg_name=constants.TASK))
    def update_task(self, current_user_id, task):
        """Does the same as update_task_by_id"""

        set_deadline_none = not task.deadline
        return self.update_task_by_id(current_user_id, task.id,
                                      task.group_id, set_deadline_none,
                                      task.deadline, task.title,
                                      task.description, task.priority)

    @services_manager_log()
    @creator_access()
    def update_subtask(self, current_user_id, task_id,
                       title=None, description=None,
                       update_child_tasks=True):
        """
        :param current_user_id: int - current user id
        :param task_id: int - id of subtask to update
        :param title: str - new title
        :param description: str - new description
        :param update_child_tasks: boolean - set to True if you want the subtasks of subtask to be updated
        :return: Task - updated subtask

        Exceptions raised:
        ModelsObjectNotFoundError - if subtask with task_id doesn't exist
        AccessDeniedError - if current user is not creator of the task to update
        """
        task = self.get_task_by_id(current_user_id, task_id)
        parent_task = self.get_task_by_id(current_user_id, task.parent_task_id)

        kwargs = {constants.GROUP_ID: parent_task.group_id,
                  constants.DEADLINE: parent_task.deadline,
                  constants.PRIORITY: parent_task.priority,
                  constants.REPEAT: parent_task.repeat,
                  constants.REPEAT_PERIOD: parent_task.repeat_period}
        if title:
            kwargs[constants.TITLE] = title
        if description:
            kwargs[constants.DESCRIPTION] = description

        task = self.storage.update(models.Task, task_id, **kwargs)

        if update_child_tasks:
            for subtask in self.get_subtasks(current_user_id, task.id):
                self.update_subtask(current_user_id, subtask.id)

        return task

    @services_manager_log()
    def get_tasks(self, current_user_id, sort_key=None,
                  task_id=None, creator_id=None, group_id=None,
                  parent_task_id=None, root_task=False,
                  without_deadline=False, deadline=None, title=None,
                  priority=None, status=None, repeat=None):
        """

        :param current_user_id: int - current user
        :param sort_key: function - use to tell how to order tasks
        Get tasks..
        :param task_id: int - with such id
        :param creator_id: int - created by this user
        :param group_id: int - in this group
        :param parent_task_id: int - that are subtasks of this task
        :param root_task: boolean - that are not subtasks
        :param without_deadline: boolean - that have no deadline
        :param deadline: datetime - with such deadline
        :param title: str - with such title
        :param priority: Priority - with such priority
        :param status: Status - with such status
        :param repeat: boolean - repeatable
        :return: list of Task objects - return tasks that current user has viewer access to
        """

        # form a dict of filters
        kwargs = {}
        if task_id:
            kwargs[constants.OBJ_ID] = task_id
        if creator_id:
            kwargs[constants.CREATOR_ID] = creator_id
        if group_id:
            kwargs[constants.GROUP_ID] = group_id
        if parent_task_id or root_task:
            if root_task:
                parent_task_id = None
            kwargs[constants.PARENT_TASK_ID] = parent_task_id
        if deadline or without_deadline:
            if without_deadline:
                deadline = None
            kwargs[constants.DEADLINE] = deadline
        if title:
            kwargs[constants.TITLE] = title
        if priority:
            kwargs[constants.PRIORITY] = priority
        if status:
            kwargs[constants.STATUS] = status
        if repeat is not None:
            kwargs[constants.REPEAT] = repeat

        # get only those that current user has viewer access to
        tasks = list(filter(lambda t: self.is_viewer(current_user_id, t.id),
                            self.storage.get(models.Task, **kwargs)))

        # ordering of tasks before issuing
        tasks.sort(key=SortKeyGetter.DEFAULT)
        if sort_key:
            tasks.sort(key=sort_key)

        return tasks

    @services_manager_log()
    @viewer_access()
    def get_task_by_id(self, current_user_id, task_id):
        """
        :param current_user_id: int - current user
        :param task_id: int - id of task to get
        :return: Task - found task

        Exceptions raised:
        ModelsObjectNotFoundError - if task doesn't exist
        AccessDeniedError - if current user doesn't have viewer access to the task
        """
        return self.storage.get_or_raise_error(models.Task, id=task_id)[0]

    @services_manager_log()
    @executor_access()
    @change_last_updated_time
    def complete_task(self, current_user_id, task_id):
        """
        :param current_user_id: int - current user
        :param task_id: int - task to complete
        :return: Task or None - new task if completed task was repeatable or None

        Exceptions raised:
        PermissionError - if task is not pending
        ModelsObjectNotFoundError - if task is not found
        AccessDeniedError - if current user doesn't have executor access to the task

        Side effects:
        - reminders are all marked either canceled or worked
        - subtasks are completed
        """

        if (self.get_task_by_id(current_user_id, task_id).status
                is not models.Status.PENDING):
            raise PermissionError('You cannot complete not pending task.')

        # mark as done
        task = self.storage.update(request_type=models.Task, obj_id=task_id,
                                   status=models.Status.DONE)

        # if repeatable
        new_task = self.handle_repeat(current_user_id, task)

        # handle reminders
        now = datetime.datetime.now()
        for reminder in self.get_reminders(current_user_id, task_id):
            if reminder.time > now:
                self.storage.update(models.Reminder, reminder.id,
                                    status=models.ReminderStatus.CANCELED)
            else:
                self.storage.update(models.Reminder, reminder.id,
                                    status=models.ReminderStatus.WORKED)

        # complete subtasks
        for subtask in self.get_subtasks(current_user_id, task.id):
            if (self.get_task_by_id(current_user_id, subtask.id).status
                    is models.Status.PENDING):
                self.complete_task(current_user_id, subtask.id)
        return new_task

    @services_manager_log()
    @manager_access()
    @change_last_updated_time
    def cancel_task(self, current_user_id, task_id):
        """
        :param current_user_id: int - current user
        :param task_id: int - task to cancel
        :return: Task or None - updated task

        Exceptions raised:
        PermissionError - if task is not pending
        ModelsObjectNotFoundError - if task is not found
        AccessDeniedError - if current user doesn't have manager access to the task

        Side effects:
        - pending reminders are all marked canceled
        - subtasks are canceled
        """

        if (self.get_task_by_id(current_user_id, task_id).status
                is not models.Status.PENDING):
            raise PermissionError('You cannot cancel not pending task.')

        # mark as canceled
        task = self.storage.update(models.Task, task_id,
                                   status=models.Status.CANCELED)

        # cancel reminders
        for reminder in self.get_reminders(current_user_id, task_id):
            self.storage.update(models.Reminder, reminder.id,
                                status=models.ReminderStatus.CANCELED)

        # cancel subtasks
        for subtask in self.get_subtasks(current_user_id, task.id):
            if (self.get_task_by_id(current_user_id, subtask.id).status
                    is models.Status.PENDING):
                self.cancel_task(current_user_id, subtask.id)
        return task

    @services_manager_log()
    @manager_access()
    @change_last_updated_time
    def resume_task(self, current_user_id, task_id):
        """
        :param current_user_id: int - current user
        :param task_id: int - task to resume
        :return: Task or None - updated task

        Exceptions raised:
        PermissionError - if task is not canceled
        ModelsObjectNotFoundError - if task is not found
        AccessDeniedError - if current user doesn't have manager access to the task

        Side effects:
        - reminders are marked pending if they are not overdue
        - subtasks are resumed
        """

        if (self.get_task_by_id(current_user_id, task_id).status
                is not models.Status.CANCELED):
            raise PermissionError('You cannot resume not canceled task.')

        # mark as pending
        task = self.storage.update(models.Task, task_id,
                                   status=models.Status.PENDING)

        # handle reminders
        now = datetime.datetime.now()
        for reminder in self.get_reminders(current_user_id, task_id):
            if reminder.time > now:
                self.storage.update(models.Reminder, reminder.id,
                                    status=models.ReminderStatus.PENDING)
                continue
            self.storage.update(models.Reminder, reminder.id,
                                status=models.ReminderStatus.WORKED)

        # resume subtasks
        for subtask in self.get_subtasks(current_user_id, task.id):
            if (self.get_task_by_id(current_user_id, subtask.id).status
                    is models.Status.CANCELED):
                self.resume_task(current_user_id, subtask.id)

        return task

    @services_manager_log()
    @creator_access()
    @change_last_updated_time
    def set_repeat(self, current_user_id, task_id, repeat_period=None):
        """
        :param current_user_id: int - current user
        :param task_id: int - id of task to set repeat to
        :param repeat_period: rrule - rule to manage repeat (count=2!)
        :return: Task - updated task

        Exceptions raised:
        PermissionError - if task is not root task
        ModelsObjectNotFoundError - if task not found
        ValueError - if repeat_period is None and task.deadline is not
        AccessDeniedError - if current user doesn't have creator access to the task

        Side effects:
        - subtasks are updated
        """

        task = self.get_task_by_id(current_user_id, task_id)

        if task.parent_task_id is not None:
            raise PermissionError('You cannot change repeat info of a subtask.')

        if not repeat_period and task.deadline:
            raise ValueError('Repeat period cannot be None when deadline is specified.')

        if not task.deadline:
            repeat_period = None

        task = self.storage.update(models.Task, task_id,
                                   repeat=True, repeat_period=repeat_period)

        for subtask in self.get_subtasks(current_user_id, task_id):
            self.update_subtask(current_user_id, subtask.id)

        return task

    @services_manager_log()
    @creator_access()
    @change_last_updated_time
    def unset_repeat(self, current_user_id, task_id):
        """
        :param current_user_id: int - current user
        :param task_id: int - task to unset repeat from
        :return: Task - updated task

        Exceptions raised:
        PermissionError - if task is not root task
        ModelsObjectNotFoundError - if task not found
        AccessDeniedError - if current user doesn't have creator access to the task

        Side effects:
        - subtasks are updated
        """

        task = self.get_task_by_id(current_user_id, task_id)

        if task.parent_task_id is not None:
            raise PermissionError('You cannot change repeat info of a subtask.')

        task = self.storage.update(models.Task, task_id,
                                   repeat=False, repeat_period=None)

        for subtask in self.get_subtasks(current_user_id, task_id):
            self.update_subtask(current_user_id, subtask.id)

        return task

    @services_manager_log()
    @viewer_access(get_task_id=lambda arg: arg.id,
                   task_argument_getter=
                   target_argument_getter(index=0, arg_name=constants.TASK))
    def copy_and_shift_task(self, current_user_id, task, new_deadline):
        """Copies any task and creates root task ( a copy) with new_deadline
        :param current_user_id: int - current user
        :param task: Task - task to copy and shift
        :param new_deadline: datetime or None - new deadline to set
        :return: Task - copy

        Exceptions raised:
        AccessDeniedError - if current user doesn't have viewer access to the task

        Side effects:
        - if task is a subtask it becomes a root task
        """

        new_task = self.create_task(current_user_id, task.group_id,
                                    task.title, task.description,
                                    new_deadline, task.priority)
        return self.storage.update(models.Task, new_task.id,
                                   repeat=task.repeat, repeat_period=task.repeat_period)

    @services_manager_log()
    @viewer_access(get_task_id=lambda arg: arg.id,
                   task_argument_getter=
                   target_argument_getter(index=0, arg_name=constants.SUBTASK))
    def copy_and_move_subtask(self, current_user_id, subtask, new_parent_task_id,
                              copy_subtasks=True):
        """Copies and moves subtask
        :param current_user_id: int - current user
        :param subtask: Task - subtask to copy and move
        :param new_parent_task_id: int - new parent task
        :param copy_subtasks: boolean - if True moves the whole subtree of a subtask
        :return: Task - copy of subtask

        Exceptions raised:
        AccessDeniedError - if current user doesn't have viewer access to the subtask
        """

        new_subtask = self.create_subtask(current_user_id, new_parent_task_id,
                                          subtask.title, subtask.description)
        if not copy_subtasks:
            return new_subtask

        for subsubtask in self.get_subtasks(current_user_id, subtask.id):
            self.copy_and_move_subtask(current_user_id, subsubtask,
                                       new_parent_task_id=new_subtask.id)

        return new_subtask

    @services_manager_log()
    def get_overdue_objects(self, current_user_id,
                            task_id=None, creator_id=None, group_id=None):
        """Handles tasks that current user has viewer access to and that passed filters
        :param current_user_id: int - current user
        :param task_id: int - id of task to check for being overdue and handle reminders of
        :param creator_id: int - whose tasks to handle
        :param group_id: int - handle tasks from the group
        :return: () - list of failed tasks, list of new tasks, worked reminders

        Side effects:
        - overdue tasks are marked failed and reside in the first tuple element
        - if failed tasks are repeatable they and their subtrees are copied and tasks are returned in the second element
        - overdue reminders are returned as the third tuple element
        """

        kwargs = {constants.TASK_ID: task_id,
                  constants.CREATOR_ID: creator_id,
                  constants.GROUP_ID: group_id}
        failed_tasks, new_tasks = self.handle_overdue_pending_tasks(current_user_id,
                                                                    **kwargs)
        worked_reminders = self.handle_overdue_pending_reminders(current_user_id,
                                                                 **kwargs)
        return failed_tasks, new_tasks, worked_reminders

    @services_manager_log()
    def handle_overdue_pending_tasks(self, current_user_id, task_id=None,
                                     creator_id=None, group_id=None):
        tasks = self.get_tasks(current_user_id, task_id=task_id,
                               creator_id=creator_id, group_id=group_id)
        overdue_tasks = []
        newly_created_tasks = []
        current_time = datetime.datetime.now()
        for task in tasks:
            if not (task.deadline and task.deadline < current_time
                    and task.status is models.Status.PENDING):
                continue
            task = self.storage.update(models.Task, task.id,
                                       status=models.Status.FAILED)
            overdue_tasks.append(task)
            new_task = self.handle_repeat(current_user_id, task)
            if new_task:
                newly_created_tasks.append(new_task)
        return overdue_tasks, newly_created_tasks

    @services_manager_log()
    def handle_overdue_pending_reminders(self, current_user_id,
                                         task_id=None, creator_id=None, group_id=None):
        tasks = self.get_tasks(current_user_id, task_id=task_id,
                               creator_id=creator_id, group_id=group_id)

        reminders = []
        for task in tasks:
            reminders.extend(self.get_reminders(current_user_id, task.id))

        overdue_reminders = []
        now = datetime.datetime.now()
        for reminder in reminders:
            if not (reminder.status is models.ReminderStatus.PENDING
                    and reminder.time < now):
                continue
            self.storage.update(models.Reminder, reminder.id,
                                status=models.ReminderStatus.WORKED)
            overdue_reminders.append(reminder)
        return overdue_reminders

    @services_manager_log(level=logging.DEBUG)
    def handle_repeat(self, current_user_id, task):

        def get_next_datetime_and_update_rule(rule):
            next_datetime = list(rule)[1]
            new_rule = rule.replace(dtstart=next_datetime)
            return next_datetime, new_rule

        if not (task.repeat and task.parent_task_id is None):
            return

        new_deadline = task.deadline
        if task.deadline:
            new_deadline, task.repeat_period = \
                get_next_datetime_and_update_rule(task.repeat_period)

        copy = self.copy_and_shift_task(current_user_id, task, new_deadline)
        for subtask in self.get_subtasks(current_user_id, task.id):
            self.copy_and_move_subtask(current_user_id, subtask,
                                       new_parent_task_id=copy.id)

        if not task.deadline:
            return copy

        for reminder in self.get_reminders(current_user_id, task.id):
            if reminder.status is models.ReminderStatus.CANCELED:
                continue
            self.set_reminder(current_user_id, copy.id,
                              (reminder.time + (new_deadline -
                               task.deadline)))

        return copy

    @services_manager_log()
    @creator_access()
    def set_reminder(self, current_user_id, task_id, at=None, before=None):
        """
        :param current_user_id: int - current user
        :param task_id: int - task to set reminder to
        :param at: datetime or None - when reminder should work
        :param before: timedelta or None - how much time in advance
        :return: Reminder - created reminsder

        Exceptions raised:
        ValueError - if no at and no deadline, if at is greater than deadline
        ModelsObjectNotFoundError - if task with task_id is not found
        AccessDeniedError - if current user is not creator of the task
        """

        task = self.get_task_by_id(current_user_id, task_id)

        if not at and not task.deadline:
            raise ValueError('at cannot be None when no deadline is specified.')

        if task.deadline and at and at > task.deadline:
            raise ValueError('at parameter cannot be greater than deadline for task.')

        if not at and not before:
            at = task.deadline
        elif not at:
            at = task.deadline - before

        return self.storage.create(models.Reminder, task_id, time=at,
                                   status=models.ReminderStatus.PENDING)

    @services_manager_log()
    def dismiss_reminder(self, current_user_id, reminder_id):
        """
        :param current_user_id: int  - current user id
        :param reminder_id: int - id of the reminder to delete
        :return: NoneType

        Exceptions raised:
        AccessDeniedError - if current user is not creator
        """

        is_creator = self.is_creator(current_user_id,
                                     self.storage.get_or_raise_error(models.Reminder,
                                                                     id=reminder_id)[0].task_id)
        if (not is_creator):
            raise models.AccessDeniedError('Only creator has access to such operation.')

        self.storage.delete(models.Reminder, reminder_id)

    @services_manager_log()
    @viewer_access()
    def get_reminders(self, current_user_id, task_id):
        """
        :param current_user_id: int - current user
        :param task_id: int - task to get reminders of
        :return: list of Reminder objects - reminders of the task

        Exceptions raised:
        AccessDeniedError - if current user doesn't have viewer access to the task
        """

        return self.storage.get(models.Reminder, task_id=task_id)

    @services_manager_log()
    @manager_access()
    def grant_permission(self, current_user_id, task_id, user_id,
                         access_type=models.AccessType.VIEWER):
        """
        :param current_user_id: int - granter
        :param task_id: int - task the permission to which is granted
        :param user_id: int - user to which the permission is granted
        :param access_type: AccessType - the type of access
        :return: NoneType

        Exceptions raised:
        AccessDeniedError - if current user doesn't have manager access to the task,
        or permission is being granted to creator or manager is trying to grant manager permission
        """

        if self.is_creator(user_id, task_id):
            raise models.AccessDeniedError('You are trying to grant creator with rights.')

        if (access_type.value == models.AccessType.MANAGER.value
                and not self.is_creator(current_user_id, task_id)):
            raise models.AccessDeniedError('You cannot grant such rights on this task.')

        if not self.has_permission_of_type(task_id, user_id, access_type):
            self.storage.create(models.Permission, task_id, user_id, access_type)

    @services_manager_log()
    @manager_access()
    def withdraw_permission(self, current_user_id, task_id,
                            user_id, access_type):
        """
        :param current_user_id: int - who withdraws
        :param task_id: int - task the permission to which is withdrawn
        :param user_id: int - user from which permission is withdrawn
        :param access_type: AccessType - the type of access
        :return: NoneType

        Exceptions raised:
        AccessDeniedError - if current user doesn't have manager access to the task,
        or permission is being withdrawn from creator or manager is trying to withdraw manager permission
        """

        if self.is_creator(user_id, task_id):
            raise models.AccessDeniedError('You are trying to withdraw rights from creator.')

        if (access_type.value == models.AccessType.MANAGER.value
                and not self.is_creator(current_user_id, task_id)):
            raise models.AccessDeniedError('You cannot withdraw such permissions on this task.')

        if not self.has_permission_of_type(task_id, user_id, access_type):
            return

        permission = self.storage.get(models.Permission, user_id=user_id,
                                      task_id=task_id, access_type=access_type)[0]
        self.storage.delete(models.Permission, permission.id)

    @services_manager_log()
    def withdraw_all_permissions(self, current_user_id, task_id, user_id):
        """Does the same as withdraw_permission except it withdraws all types of permission from the user on the task"""

        self.withdraw_permission(current_user_id, task_id,
                                 user_id, models.AccessType.VIEWER)
        self.withdraw_permission(current_user_id, task_id,
                                 user_id, models.AccessType.EXECUTOR)
        self.withdraw_permission(current_user_id, task_id,
                                 user_id, models.AccessType.MANAGER)

    @services_manager_log()
    @manager_access()
    def get_permissions(self, current_user_id, task_id, permission_id=None,
                        user_id=None, access_type=None):
        """
        :param current_user_id: int - current user
        :param task_id: int - task to get permission to
        Get permissions ..
        :param permission_id: int - with such id
        :param user_id: int - granted to user
        :param access_type: AccessType - of the type
        :return: list of Permission objects

        Exceptions raised:
        AccessDeniedError - if current user doesn't have manager access to the task
        """
        kwargs = {}
        if permission_id:
            kwargs[constants.OBJ_ID] = permission_id
        if task_id:
            kwargs[constants.TASK_ID] = task_id
        if user_id:
            kwargs[constants.USER_ID] = user_id
        if access_type:
            kwargs[constants.ACCESS_TYPE] = access_type
        return self.storage.get(models.Permission, **kwargs)

    @services_manager_log()
    def has_permission_of_type(self, task_id, user_id, access_type):
        return bool(self.storage.get(models.Permission, task_id=task_id,
                                     user_id=user_id, access_type=access_type))

    @services_manager_log(level=logging.DEBUG)
    def is_creator(self, user_id, task_id):
        task = self.storage.get_or_raise_error(models.Task, id=task_id)[0]
        return task.creator_id == user_id

    @services_manager_log(level=logging.DEBUG)
    def is_manager(self, user_id, task_id):
        task = self.storage.get_or_raise_error(models.Task, id=task_id)[0]
        result = (self.has_permission_of_type(task_id, user_id, models.AccessType.MANAGER) or
                  self.is_creator(user_id, task_id))
        if task.parent_task_id:
            result = result or self.is_manager(user_id, task.parent_task_id)
        return result

    @services_manager_log(level=logging.DEBUG)
    def is_executor(self, user_id, task_id):
        task = self.storage.get_or_raise_error(models.Task, id=task_id)[0]
        result = (self.has_permission_of_type(task_id, user_id, models.AccessType.EXECUTOR) or
                  self.is_manager(user_id, task_id))
        if task.parent_task_id:
            result = result or self.is_executor(user_id, task.parent_task_id)
        return result

    @services_manager_log(level=logging.DEBUG)
    def is_viewer(self, user_id, task_id):
        task = self.storage.get_or_raise_error(models.Task, id=task_id)[0]
        result = (self.has_permission_of_type(task_id, user_id, models.AccessType.VIEWER) or
                  self.is_executor(user_id, task_id))
        if task.parent_task_id:
            result = result or self.is_viewer(user_id, task.parent_task_id)
        return result

    @services_manager_log()
    def create_group(self, current_user_id, title):
        """
        :param current_user_id: int - group creator
        :param title: str - name for the group
        :return: Group - group
        """

        return self.storage.create(models.Group, current_user_id, title)

    @services_manager_log()
    @group_creator_access()
    def delete_group(self, current_user_id, group_id):
        """
        :param current_user_id: int - current user
        :param group_id: - id of the group to delete
        :return: NoneType

        Exceptions raised:
        PermissionError - if trying to delete not empty (populated) group
        AccessDeniedError - if current user is not creator
        """

        if self.get_group_tasks(current_user_id, group_id):
            raise PermissionError('It is forbidden to delete non-empty groups.')

        self.storage.delete(models.Group, group_id)

    @services_manager_log()
    @group_creator_access()
    def update_group(self, current_user_id, group_id, title=None):
        """
        :param current_user_id: int - current user
        :param group_id: int - group to update
        :param title: str - new name
        :return: Group - updated group

        Exceptions raised:
        AccessDeniedError - if current user doesn't have creator access to group
        """

        kwargs = {}
        if title:
            kwargs[constants.TITLE] = title
        return self.storage.update(models.Group, group_id, **kwargs)

    @services_manager_log()
    def get_groups(self, current_user_id, group_id=None, title=None):
        """
        :param current_user_id: - current user
        Get groups ...
        :param group_id: int - with such id
        :param title: str - with such title
        :return: list of Group objects

        Side effects:
        - gets groups current user is creator of
        """

        kwargs = {}
        if group_id:
            kwargs[constants.OBJ_ID] = group_id
        if title:
            kwargs[constants.TITLE] = title
        kwargs[constants.CREATOR_ID] = current_user_id

        groups = self.storage.get(models.Group, **kwargs)

        # ordering
        groups.sort(key=lambda g: g.title)

        return groups

    @services_manager_log()
    @group_creator_access()
    def get_group_by_id(self, current_user_id, group_id):
        """
        :param current_user_id: int - current user
        :param group_id: int - get group by id
        :return: Group

        Exceptions raised:
        ModelsObjectNotFoundError - if group doesn't exist
        """

        return self.storage.get_or_raise_error(models.Group, id=group_id)[0]

    @services_manager_log()
    @group_creator_access()
    def get_group_tasks(self, current_user_id, group_id):
        """
        :param current_user_id: int - current user
        :param group_id: int - get tasks of the group
        :return: list of Task objects
        """

        return self.storage.get(models.Task, group_id=group_id)

    @services_manager_log(level=logging.DEBUG)
    def is_group_creator(self, user_id, group_id):
        group = self.storage.get_or_raise_error(models.Group, id=group_id)
        return group[0].creator_id == user_id

    @services_manager_log()
    @viewer_access(task_argument_getter=
                   target_argument_getter(index=0, arg_name=constants.FROM_TASK_ID))
    @viewer_access(task_argument_getter=
                   target_argument_getter(index=1, arg_name=constants.TO_TASK_ID))
    def relate(self, current_user_id, from_task_id, to_task_id, relation_type):
        """
        :param current_user_id: int - current user
        Establish relation ..
        :param from_task_id: int - from task
        :param to_task_id: int - to task
        :param relation_type: RelationType - of the type
        :return: Relation

        Exceptions raised:
        AccessDeniedError - if current user doesn't have viewer access to both tasks
        """

        # if exists
        if self.get_relations(current_user_id, from_task_id=from_task_id,
                              to_task_id=to_task_id, relation_type=relation_type):
            return

        return self.storage.create(models.Relation, current_user_id,
                                   from_task_id, to_task_id, relation_type)

    @services_manager_log()
    def delete_relation(self, current_user_id, relation_id):
        """
        :param current_user_id: int - current user
        :param relation_id: int - relation to delete
        :return: NoneType

        Exceptions raised:
        AccessDeniedError - if current user is not creator of the relation
        ModelsNotFoundError - if relation with id doesn't exist
        """

        is_creator = (current_user_id ==
                      self.storage.get_or_raise_error(models.Relation,
                                                      id=relation_id)[0].creator_id)
        if not is_creator:
            raise models.AccessDeniedError('You cannot remove '
                                           'relation you have not created.')

        self.storage.delete(models.Relation, relation_id)

    @services_manager_log()
    def get_relations(self, current_user_id, relation_id=None,
                      from_task_id=None, to_task_id=None,
                      relation_type=None):
        """
        :param current_user_id: int - current user
        Get relations ..
        :param relation_id: int - with such id
        :param from_task_id: int - from this task
        :param to_task_id: int - to this task
        :param relation_type: RelationType - of the type
        :return: list of Relation objects

        Side effects:
        - checks for each relation if current user still has viewer access to tasks. If not, relation is deleted
        """

        kwargs = {}
        if relation_id:
            kwargs[constants.OBJ_ID] = relation_id
        if from_task_id:
            kwargs[constants.FROM_TASK_ID] = from_task_id
        if to_task_id:
            kwargs[constants.TO_TASK_ID] = to_task_id
        if relation_type:
            kwargs[constants.RELATION_TYPE] = relation_type
        kwargs[constants.CREATOR_ID] = current_user_id

        relations = self.storage.get(models.Relation, **kwargs)

        for relation in relations:
            if self.is_relation_valid(relation):
                continue
            self.delete_relation(current_user_id,
                                 relation.id)
            relations.remove(relation)

        return relations

    @services_manager_log(level=logging.DEBUG)
    def is_relation_valid(self, relation):
        try:
            self.storage.get_or_raise_error(models.Task, id=relation.from_task_id)
            self.storage.get_or_raise_error(models.Task, id=relation.to_task_id)
            if (self.is_viewer(relation.creator_id, relation.from_task_id) and
                    self.is_viewer(relation.creator_id, relation.to_task_id)):
                return True
            return False
        except models.ModelObjectNotFoundError:
            return False
