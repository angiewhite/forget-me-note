import functools
import forgetmenote.models as models
import forgetmenote.str_constants as constants
from forgetmenote.models_services.helpers import target_argument_getter


def object_id_default_getter(argument):
    return argument


def default_task_argument_getter():
    return target_argument_getter(index=0, arg_name=constants.TASK_ID)


def default_group_argument_getter():
    return target_argument_getter(index=0, arg_name=constants.GROUP_ID)


def group_creator_access(get_group_id=object_id_default_getter,
                         group_argument_getter=default_group_argument_getter()):
    def decorator(func):

        @functools.wraps(func)
        def replace_func(self, current_user_id, *args, **kwargs):
            group_argument = group_argument_getter(self, args, kwargs)
            if self.is_group_creator(current_user_id,
                                     get_group_id(group_argument)):
                return func(self, current_user_id, *args, **kwargs)
            else:
                raise models.AccessDeniedError('Only group creator can perform '
                                               'this operation on the group.')

        return replace_func
    return decorator


def creator_access(get_task_id=object_id_default_getter,
                   task_argument_getter=default_task_argument_getter()):
    def decorator(func):

        @functools.wraps(func)
        def replace_func(self, current_user_id, *args, **kwargs):
            task_argument = task_argument_getter(self, args, kwargs)
            if self.is_creator(current_user_id, get_task_id(task_argument)):
                return func(self, current_user_id, *args, **kwargs)
            else:
                raise models.AccessDeniedError('Only creator has access to such operation.')

        return replace_func
    return decorator


def manager_access(get_task_id=object_id_default_getter,
                   task_argument_getter=default_task_argument_getter()):
    def decorator(func):

        @functools.wraps(func)
        def replace_func(self, current_user_id, *args, **kwargs):
            task_argument = task_argument_getter(self, args, kwargs)
            if self.is_manager(current_user_id, get_task_id(task_argument)):
                return func(self, current_user_id, *args, **kwargs)
            else:
                raise models.AccessDeniedError('Only users with manager access'
                                               ' can perform such operation.')

        return replace_func
    return decorator


def executor_access(get_task_id=object_id_default_getter,
                    task_argument_getter=default_task_argument_getter()):
    def decorator(func):

        @functools.wraps(func)
        def replace_func(self, current_user_id, *args, **kwargs):
            task_argument = task_argument_getter(self, args, kwargs)
            if self.is_executor(current_user_id, get_task_id(task_argument)):
                return func(self, current_user_id, *args, **kwargs)
            else:
                raise models.AccessDeniedError('Only users with executor access '
                                               'can perform such operation.')

        return replace_func
    return decorator


def viewer_access(get_task_id=object_id_default_getter,
                  task_argument_getter=default_task_argument_getter()):
    def decorator(func):

        @functools.wraps(func)
        def replace_func(self, current_user_id, *args, **kwargs):
            task_argument = task_argument_getter(self, args, kwargs)
            if self.is_viewer(current_user_id, get_task_id(task_argument)):
                return func(self, current_user_id, *args, **kwargs)
            else:
                raise models.AccessDeniedError('Only users with viewer access can perform'
                                               ' such operation.')

        return replace_func
    return decorator


