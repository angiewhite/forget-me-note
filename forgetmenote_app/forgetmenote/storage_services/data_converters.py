import logging
import dateutil.parser
import dateutil.rrule as rrule
import forgetmenote.models as models
import forgetmenote.str_constants as fields
from forgetmenote.log import log


def data_converters_log(level=logging.INFO):
    return log(logger_name=__name__, level=level)


@data_converters_log(level=logging.DEBUG)
def from_task_to_python_data(task):
    return {fields.OBJ_ID: task.id,
            fields.CREATOR_ID: int(task.creator_id),
            fields.PARENT_TASK_ID: task.parent_task_id,
            fields.GROUP_ID: int(task.group_id),
            fields.DEADLINE: str(task.deadline),
            fields.CREATION_TIME: str(task.creation_time),
            fields.LAST_UPDATED_TIME: str(task.last_updated_time),
            fields.TITLE: task.title,
            fields.DESCRIPTION: task.description,
            fields.PRIORITY: task.priority.name,
            fields.STATUS: task.status.name,
            fields.REPEAT: task.repeat,
            fields.REPEAT_PERIOD: from_rrule_to_python_data(task.repeat_period)}


@data_converters_log(level=logging.DEBUG)
def from_python_data_to_task(obj):
    task = models.Task(obj[fields.OBJ_ID], obj[fields.CREATOR_ID],
                       obj[fields.PARENT_TASK_ID], obj[fields.GROUP_ID],
                       obj[fields.TITLE], obj[fields.DESCRIPTION],
                       from_str_to_datetime(obj[fields.DEADLINE]),
                       models.Priority[obj[fields.PRIORITY]],
                       models.Status[obj[fields.STATUS]],
                       from_str_to_datetime(obj[fields.CREATION_TIME]),
                       from_str_to_datetime(obj[fields.LAST_UPDATED_TIME]))
    task.repeat = obj[fields.REPEAT]
    task.repeat_period = from_python_data_to_rrule(obj[fields.REPEAT_PERIOD])
    return task


@data_converters_log(level=logging.DEBUG)
def from_group_to_python_data(group):
    return {fields.OBJ_ID: group.id,
            fields.CREATOR_ID: int(group.creator_id),
            fields.TITLE: group.title}


@data_converters_log(level=logging.DEBUG)
def from_python_data_to_group(obj):
    return models.Group(obj[fields.OBJ_ID], obj[fields.CREATOR_ID], obj[fields.TITLE])


@data_converters_log(level=logging.DEBUG)
def from_reminder_to_python_data(reminder):
    return {fields.OBJ_ID: reminder.id,
            fields.TIME: str(reminder.time),
            fields.TASK_ID: reminder.task_id,
            fields.STATUS: reminder.status.name}


@data_converters_log(level=logging.DEBUG)
def from_python_data_to_reminder(obj):
    return models.Reminder(obj[fields.OBJ_ID], obj[fields.TASK_ID],
                           from_str_to_datetime(obj[fields.TIME]),
                           models.ReminderStatus[obj[fields.STATUS]])


@data_converters_log(level=logging.DEBUG)
def from_permission_to_python_data(permission):
    return {fields.OBJ_ID: permission.id,
            fields.USER_ID: permission.user_id,
            fields.TASK_ID: permission.task_id,
            fields.ACCESS_TYPE: permission.access_type.name}


@data_converters_log(level=logging.DEBUG)
def from_python_data_to_permission(obj):
    return models.Permission(obj[fields.OBJ_ID], obj[fields.TASK_ID],
                             obj[fields.USER_ID],
                             models.AccessType[obj[fields.ACCESS_TYPE]])


@data_converters_log(level=logging.DEBUG)
def from_relation_to_python_data(relation):
    return {fields.OBJ_ID: relation.id,
            fields.CREATOR_ID: relation.creator_id,
            fields.FROM_TASK_ID: relation.from_task_id,
            fields.TO_TASK_ID: relation.to_task_id,
            fields.RELATION_TYPE: relation.relation_type.name}


@data_converters_log(level=logging.DEBUG)
def from_python_data_to_relation(obj):
    return models.Relation(obj[fields.OBJ_ID],
                           obj[fields.CREATOR_ID],
                           obj[fields.FROM_TASK_ID],
                           obj[fields.TO_TASK_ID],
                           models.RelationType[obj[fields.RELATION_TYPE]])


@data_converters_log(level=logging.DEBUG)
def from_str_to_datetime(datetime_str):
    if datetime_str == fields.NONE_STR:
        return None
    return dateutil.parser.parse(datetime_str)


@data_converters_log(level=logging.DEBUG)
def from_rrule_to_python_data(rrule):
    if rrule:
        return {fields.COUNT: rrule._count,
                fields.INTERVAL: rrule._interval,
                fields.FREQUENCY: rrule._freq,
                fields.BYWEEKDAY: rrule._byweekday,
                fields.DTSTART: str(rrule._dtstart)}


@data_converters_log(level=logging.DEBUG)
def from_python_data_to_rrule(rrule_dict):
    if rrule_dict:
        return rrule.rrule(freq=rrule_dict[fields.FREQUENCY],
                           interval=rrule_dict[fields.INTERVAL],
                           count=rrule_dict[fields.COUNT],
                           byweekday=rrule_dict[fields.BYWEEKDAY],
                           dtstart=from_str_to_datetime(rrule_dict[fields.DTSTART]))




