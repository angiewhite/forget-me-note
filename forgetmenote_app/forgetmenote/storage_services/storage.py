import logging
import os
import json
import forgetmenote.models as models
import forgetmenote.storage_services.data_converters as converters
import forgetmenote.str_constants as constants
from forgetmenote.log import log


def storage_log(level=logging.INFO):
    return log(logger_name=__name__, level=level)


FROM_TYPE_CONVERTERS = {models.Task: converters.from_task_to_python_data,
                        models.Group: converters.from_group_to_python_data,
                        models.Reminder: converters.from_reminder_to_python_data,
                        models.Permission: converters.from_permission_to_python_data,
                        models.Relation: converters.from_relation_to_python_data}

TO_TYPE_CONVERTERS = {models.Task: converters.from_python_data_to_task,
                      models.Group: converters.from_python_data_to_group,
                      models.Reminder: converters.from_python_data_to_reminder,
                      models.Permission: converters.from_python_data_to_permission,
                      models.Relation: converters.from_python_data_to_relation}


def create_if_not_exists(path):
    if not os.path.exists(path):
        with open(path, 'w') as file:
            file.write('[]')
    return path


class Storage:
    """Type that contains storage manipulation logic.

    It provides the following set of public methods:
    create - use to create corresponding entry for the object in the storage
    delete - deletes object in the storage
    update - updates object's info in the storage
    get - returns objects that satisfy provided criteria
    get_or_raise_error - does essentially the same as get method except it raises error when no objects found to satisfy criteria
    """

    @storage_log()
    def __init__(self, db_files_folder_path):
        """Initializes storage object.

        Pass it a path str object that is pointing at the folder where the given storage will reside.
        Then the storage initializes necessary files in the folder that correspond to each model's storage.
        """
        self.db_files = {models.Task:
                             create_if_not_exists(os.path.join(db_files_folder_path,
                                                               'tasks.json')),
                         models.Group:
                             create_if_not_exists(os.path.join(db_files_folder_path,
                                                               'groups.json')),
                         models.Reminder:
                             create_if_not_exists(os.path.join(db_files_folder_path,
                                                               'reminders.json')),
                         models.Permission:
                             create_if_not_exists(os.path.join(db_files_folder_path,
                                                               'permissions.json')),
                         models.Relation:
                             create_if_not_exists(os.path.join(db_files_folder_path,
                                                               'relations.json'))}

    @storage_log(level=logging.DEBUG)
    def create(self, request_type, *args, **kwargs):
        """Method which creates object of a type and saves it to storage

        :param request_type: type - type of object to create
        :param args: tuple - argument to initialize new object (see object's init details)
        :param kwargs: dict - keyword arguments that do the same service
        :return: request_type - created object

        Provide all the arguments except for id since id is generated by the storage.
        """
        objects = self.get(request_type)  # retrieves all objects of the type

        new_id = max(map(lambda o: o.id, objects), default=0) + 1  # generates unique id
        new_object = request_type(new_id, *args, **kwargs)  # builds new object

        objects.append(new_object)  # appends new object to other objects of the type
        self._save(request_type, objects)  # dumps all of them to storage

        return new_object

    @storage_log(level=logging.DEBUG)
    def delete(self, request_type, obj_id):
        """Use to delete particular object from storage.

        :param request_type: type - type of the object to delete
        :param obj_id: int - id of the object to delete
        :return: NoneType
        """
        objects = self.get(request_type)   # retrieves the list of objects of the type

        # searches for object to delete
        # if fails to find throws exception
        for o in objects:
            if o.id == obj_id:
                object_to_delete = o
                break
        else:
            raise models.ModelObjectNotFoundError(request_type, obj_id)

        objects.remove(object_to_delete)  # removes the object from the list of suchlike

        self._save(request_type, objects)  # dumps all remaining objects to storage

    @storage_log(level=logging.DEBUG)
    def update(self, request_type, obj_id, **kwargs):
        """Updates object of the provided type by assigning new value to fields provided as kwargs keys.

        :param request_type: type - type of the object to update
        :param obj_id: int - id of the object to update
        :param kwargs: dict - "field-new value" pairs
        :return: request_type - updated object
        """
        objects = self.get(request_type)  # retrieves all objects of the type

        # searches for object to update
        # if fails to find throws exception
        for o in objects:
            if o.id == obj_id:
                obj = o
                break
        else:
            raise models.ModelObjectNotFoundError(request_type, obj_id)

        # updates object's attributes provided by kwargs keys
        # raises KeyError if object misses any attribute
        obj.__dict__.update(kwargs)
        self._save(request_type, objects)  # dumps all objects to storage

        return obj

    @storage_log(level=logging.DEBUG)
    def get(self, request_type, **kwargs):
        """Use it to read data that satisfies certain criteria from storage.

        :param request_type: type - type of objects to look for
        :param kwargs: dict - provided criteria as "field-value" pairs
        :return: list of request_type objects
        """
        with open(self.db_files[request_type], 'r') as file:
            python_objects = json.load(file)  # retrieves objects as python dicts

        # converts objects to request_type
        # filters out objects that do not match kwargs constraints
        # raises KeyError if an object misses an attribute
        objects = []
        for obj in python_objects:
            obj = TO_TYPE_CONVERTERS[request_type](obj)
            for k in kwargs:
                if obj.__dict__[k] != kwargs[k]:
                    break
            else:
                objects.append(obj)

        # returns kwargs-conforming objects
        return objects

    @storage_log(level=logging.DEBUG)
    def get_or_raise_error(self, request_type, **kwargs):
        """Same as get except it raises error when nothing is found."""
        result = self.get(request_type, **kwargs)

        if result:
            return result

        # if object was not found raises error
        look_up_id = kwargs.get(constants.OBJ_ID, '?')
        raise models.ModelObjectNotFoundError(request_type, look_up_id)

    @storage_log(level=logging.DEBUG)
    def _save(self, request_type, objects):
        # makes a list of json-serializable objects
        # from provided list of objects of the request_type
        python_objects = list(map(lambda o: FROM_TYPE_CONVERTERS[request_type](o),
                                  objects))

        with open(self.db_files[request_type], 'w') as file:
            json.dump(python_objects, file, indent=4)  # dumps the list to a file
