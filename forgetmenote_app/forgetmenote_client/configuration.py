import os
import logging.config
import json
import configparser

def get_config_path():
    return os.path.join(os.path.dirname(__file__), 'config.ini')


def read_configuration():
    logging.info('{} - Reading configuration'.format(__name__))
    config = configparser.ConfigParser()
    config.read(get_config_path())
    return config


def get_current_user_id():
    config = read_configuration()
    return int(config['current_user']['id'])


def get_storage_path():
    config = read_configuration()
    storage_config = config['storage']
    directory = os.path.join(os.path.split(os.path.dirname(__file__))[0],
                             storage_config['folder_name'])
    return directory


def initialize_loggers(logging_config_path):
    logging_config_path = os.path.join(os.path.dirname(__file__),
                                       logging_config_path)
    with open(logging_config_path, 'r') as file:
        config = json.load(file)
    logging.config.dictConfig(config)


def configure_logging():
    configuration = read_configuration()
    if configuration['logging'].getboolean('mode'):
        logging.info('{} - Configuring logging'.format(__name__))
        initialize_loggers(configuration['logging']['config_file_path'])
    else:
        logging.basicConfig(handlers=[logging.NullHandler()])


def save_configuration(config):
    logging.info('{} - Changing configuration'.format(__name__))
    with open(get_config_path(), 'w') as file:
        config.write(file)