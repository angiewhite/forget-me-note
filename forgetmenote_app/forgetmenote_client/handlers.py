import dateutil.rrule as rrule
from forgetmenote import Status
from forgetmenote import str_constants
from forgetmenote_client import configuration


def create_task(manager, current_user_id, args):
    kwargs = {}
    if args.description:
        kwargs[str_constants.DESCRIPTION] = args.description
    if args.deadline:
        kwargs[str_constants.DEADLINE] = args.deadline
    if args.priority:
        kwargs[str_constants.PRIORITY] = args.priority
    return manager.create_task(current_user_id,
                               args.group_id,
                               args.title, **kwargs)


def create_group(manager, current_user_id, args):
    return manager.create_group(current_user_id, args.title)


def create_subtask(manager, current_user_id, args):
    kwargs = {}
    if args.description:
        kwargs[str_constants.DESCRIPTION] = args.description
    return manager.create_subtask(current_user_id, args.parent_task_id,
                                  args.title, **kwargs)


def delete_task(manager, current_user_id, args):
    return manager.delete_task(current_user_id, args.task_id)


def delete_group(manager, current_user_id, args):
    return manager.delete_group(current_user_id, args.group_id)


def dismiss_reminder(manager, current_user_id, args):
    return manager.dismiss_reminder(current_user_id, args.reminder_id)


def get_subtasks(manager, current_user_id, args):
    return manager.get_subtasks(current_user_id, args.parent_task_id)


def get_grouped_tasks(manager, current_user_id, args):
    return manager.get_group_tasks(current_user_id, args.group_id)


def get_groups(manager, current_user_id, args):
    kwargs = {}
    if args.group_id:
        kwargs[str_constants.GROUP_ID] = args.group_id
    if args.creator_id:
        kwargs[str_constants.CREATOR_ID] = args.creator_id
    if args.title:
        kwargs[str_constants.TITLE] = args.title
    return manager.get_groups(current_user_id, **kwargs)


def get_group(manager, current_user_id, args):
    return manager.get_group_by_id(current_user_id, args.group_id)


def get_tasks(manager, current_user_id, args):
    kwargs = {}
    if args.task_id:
        kwargs[str_constants.TASK_ID] = args.task_id
    if args.creator_id:
        kwargs[str_constants.CREATOR_ID] = args.creator_id
    if args.group_id:
        kwargs[str_constants.GROUP_ID] = args.group_id
    if args.parent_task_id:
        if args.parent_task_id == 0:
            kwargs[str_constants.ROOT_TASK] = True
        else:
            kwargs[str_constants.PARENT_TASK_ID] = args.parent_task_id
    if args.deadline:
        if args.deadline == 0:
            kwargs[str_constants.WITHOUT_DEADLINE] = True
        else:
            kwargs[str_constants.DEADLINE] = args.deadline
    if args.title:
        kwargs[str_constants.TITLE] = args.title
    if args.priority:
        kwargs[str_constants.PRIORITY] = args.priority
    if args.repeat:
        kwargs[str_constants.REPEAT] = args.repeat

    if args.all and args.status:
        raise Exception('You cannot get all tasks '
                        'if you use status option.')

    if args.sort_mode:
        kwargs[str_constants.SORT_KEY] = args.sort_mode

    filtered_tasks = manager.get_tasks(current_user_id, **kwargs)

    if args.all:
        return filtered_tasks

    if not args.status:
        args.status = Status.PENDING

    return [task for task in filtered_tasks if task.status is args.status]


def get_task(manager, current_user_id, args):
    return manager.get_task_by_id(current_user_id, args.task_id)


def update_group(manager, current_user_id, args):
    return manager.update_group(current_user_id, args.group_id,
                                args.title)


def update_task(manager, current_user_id, args):
    task = manager.get_task_by_id(current_user_id, args.task_id)
    if args.group_id:
        task.group_id = args.group_id
    if args.deadline is not None:
        if args.deadline == 0:
            task.deadline = None
        else:
            task.deadline = args.deadline
    if args.title:
        task.title = args.title
    if args.description:
        task.description = args.description
    if args.priority:
        task.priority = args.priority
    return manager.update_task(current_user_id, task)


def update_subtask(manager, current_user_id, args):
    kwargs = {}
    if args.title:
        kwargs[str_constants.TITLE] = args.title
    if args.description:
        kwargs[str_constants.DESCRIPTION] = args.description
    return manager.update_subtask(current_user_id, args.subtask_id,
                                  update_child_tasks=False, **kwargs)


def copy_task(manager, current_user_id, args):
    task_to_copy = manager.get_task_by_id(current_user_id, args.task_id)
    new_deadline = task_to_copy.deadline
    if args.new_deadline is not None:
        new_deadline = args.new_deadline if args.new_deadline else None
    task = manager.copy_and_shift_task(current_user_id, task_to_copy,
                                       new_deadline=new_deadline)
    if not args.whole:
        return task
    for subtask in manager.get_subtasks(current_user_id, task_to_copy.id):
        manager.copy_and_move_subtask(current_user_id, subtask,
                                      new_parent_task_id=task.id)
    return task


def copy_subtask(manager, current_user_id, args):
    subtask_to_copy = manager.get_task_by_id(current_user_id, args.subtask_id)
    new_parent_task_id = subtask_to_copy.parent_task_id
    if args.new_parent_id:
        new_parent_task_id = args.new_parent_id
    return manager.copy_and_move_subtask(current_user_id, subtask_to_copy,
                                         new_parent_task_id=new_parent_task_id,
                                         copy_subtasks=args.whole)


def complete_task(manager, current_user_id, args):
    return manager.complete_task(current_user_id, args.task_id)


def cancel_task(manager, current_user_id, args):
    return manager.cancel_task(current_user_id, args.task_id)


def resume_task(manager, current_user_id, args):
    return manager.resume_task(current_user_id, args.task_id)


def relate_task(manager, current_user_id, args):
    return manager.relate(current_user_id, args.from_task_id,
                          args.to_task_id, args.relation_type)


def unrelate_task(manager, current_user_id, args):
    return manager.delete_relation(current_user_id, args.relation_id)


def get_relations(manager, current_user_id, args):
    kwargs = {}
    if args.from_task_id:
        kwargs[str_constants.FROM_TASK_ID] = args.from_task_id
    if args.to_task_id:
        kwargs[str_constants.TO_TASK_ID] = args.to_task_id
    if args.relation_id:
        kwargs[str_constants.RELATION_ID] = args.relation_id
    if args.relation_type:
        kwargs[str_constants.RELATION_TYPE] = args.relation_type
    return manager.get_relations(current_user_id,
                                 **kwargs)


def get_reminders(manager, current_user_id, args):
    return manager.get_reminders(current_user_id, args.task_id)


def set_reminder(manager, current_user_id, args):
    return manager.set_reminder(current_user_id, args.task_id,
                                at=args.at, before=args.before)


def set_repeat(manager, current_user_id, args):
    set_repeat_period = False

    freq = rrule.DAILY
    interval = 1
    byweekday = None
    rule = None

    if args.minutes:
        freq = rrule.MINUTELY
        interval = args.minutes
        set_repeat_period = True
    if args.hours:
        freq = rrule.HOURLY
        interval = args.hours
        set_repeat_period = True
    if args.days:
        freq = rrule.DAILY
        interval = args.days
        set_repeat_period = True
    if args.weeks:
        freq = rrule.WEEKLY
        interval = args.weeks
        set_repeat_period = True
    if args.months:
        freq = rrule.MONTHLY
        interval = args.months
        set_repeat_period = True
    if args.years:
        freq = rrule.YEARLY
        interval = args.years
        set_repeat_period = True
    if args.week_days:
        byweekday = [rrule.MO, rrule.TU, rrule.WE,
                     rrule.TH, rrule.FR]
        set_repeat_period = True

    if set_repeat_period:
        task = manager.get_task_by_id(current_user_id, args.task_id)
        if not task.deadline:
            raise PermissionError('You cannot set repeat period '
                                  'for task without deadline.')
        rule = rrule.rrule(freq=freq, interval=interval, byweekday=byweekday,
                           count=2, dtstart=task.deadline)

    return manager.set_repeat(current_user_id, args.task_id, rule)


def unset_repeat(manager, current_user_id, args):
    return manager.unset_repeat(current_user_id, args.task_id)


def grant_permission(manager, current_user_id, args):
    return manager.grant_permission(current_user_id, args.task_id,
                                    args.user_id, args.access_type)


def withdraw_permission(manager, current_user_id, args):
    if args.access_type:
        return manager.withdraw_permission(current_user_id, args.task_id,
                                           args.user_id, args.access_type)
    else:
        return manager.withdraw_all_permissions(current_user_id, args.task_id,
                                                args.user_id)


def get_permissions(manager, current_user_id, args):
    kwargs = {}
    if args.access_type:
        kwargs[str_constants.ACCESS_TYPE] = args.access_type
    if args.permission_id:
        kwargs[str_constants.PERMISSION_ID] = args.permission_id
    if args.user_id:
        kwargs[str_constants.USER_ID] = args.user_id
    return manager.get_permissions(current_user_id, args.task_id,
                                   **kwargs)


def login(manager, current_user_id, args):
    config = configuration.read_configuration()
    previous_user = config['current_user']['id']
    config['current_user']['id'] = str(args.user_id)
    configuration.save_configuration(config)
    return '{}->{}'.format(previous_user, args.user_id)

def who(manager, current_user_id, args):
    config = configuration.read_configuration()
    return config['current_user']['id']