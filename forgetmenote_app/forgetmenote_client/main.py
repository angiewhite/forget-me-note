import os
import logging
from forgetmenote import Storage, ServicesManager
from forgetmenote_client import parser, configuration


def build_storage(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    return Storage(directory)


def output_result(result):
    if not result:
        return

    if isinstance(result, str):
        print(result)
        return

    try:
        for entry in result:
            print(entry, end='\n\n')
    except TypeError:
        print(result, end='\n\n')


def main():
    configuration.configure_logging()
    current_user_id = configuration.get_current_user_id()
    storage = build_storage(configuration.get_storage_path())
    manager = ServicesManager(storage)

    try:
        args = parser.parse()
        logging.info('Parsed namespace - {}'.format(args))
        output_result(args.handler(manager, current_user_id, args))
    except Exception as e:
        logging.warning('{} - {}'.format(type(e).__name__, e.args[0]))
        logging.info('Error type - {}'.format(type(e).__name__))
        logging.getLogger('exception_logger').exception('Exception raised in ...')
        exit(1)

    current_user_id = configuration.get_current_user_id()
    failed_tasks, new_tasks, worked_reminders = \
        manager.get_overdue_objects(current_user_id)
    if (not failed_tasks and not new_tasks
            and not  worked_reminders):
        return

    print('You have {} failed tasks, {} new tasks and '
          '{} reminders of service'.format(len(failed_tasks),
                                           len(new_tasks),
                                           len(worked_reminders)))
    print('Wanna see? (y/n)')
    answer = input()
    if answer == 'y':
        if len(failed_tasks):
            print('Failed:')
            output_result(failed_tasks)
        if len(new_tasks):
            print('New:')
            output_result(new_tasks)
        if len(worked_reminders):
            print('Reminders:')
            output_result(worked_reminders)


if __name__ == '__main__':
    main()