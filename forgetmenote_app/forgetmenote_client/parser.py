import logging
import argparse
import datetime
import dateutil.parser
from pytimeparse.timeparse import timeparse
from forgetmenote import (Priority, Status,
                          RelationType, AccessType)
from forgetmenote import SortKeyGetter
from forgetmenote import str_constants
from forgetmenote_client import handlers


def to_datetime(datetime_str):
    if datetime_str.lower() == str_constants.NONE_STR.lower():
        return 0
    return dateutil.parser.parse(datetime_str)


def to_timedelta(timedelta_str):
    return datetime.timedelta(seconds=timeparse(timedelta_str))


def to_nullable_int(int_str):
    if int_str.lower() == str_constants.NONE_STR.lower():
        return 0
    return int(int_str)


def to_priority(priority_str):
    return Priority[priority_str.upper()]


def to_status(status_str):
    return Status[status_str.upper()]


def to_relation_type(relation_str):
    return RelationType[relation_str.upper()]


def to_access_type(access_type_str):
    return AccessType[access_type_str.upper()]


SORT_KEYS = {'default': SortKeyGetter.DEFAULT,
             'priority': SortKeyGetter.PRIORITY,
             'deadline': SortKeyGetter.DEADLINE,
             'status': SortKeyGetter.STATUS}


def to_sort_mode(sort_mode_str):
    return SORT_KEYS[sort_mode_str]


def print_help_and_exit(parser):
    def help_printer(manager, current_user_id, args):
        parser.print_help()
        exit(1)
    return help_printer


def add_task_subparsers(task_parser):
    task_subparsers = task_parser.add_subparsers()

    create_task_subparser = task_subparsers.add_parser('create', help='use to create task')
    create_task_subparser.add_argument('group_id', type=int,
                                       help='id of the group a task '
                                            'belongs to')
    create_task_subparser.add_argument('title', help='short string '
                                                     'describing a task')
    create_task_subparser.add_argument('-d', '--description',
                                       help='more detailed info on the task')
    create_task_subparser.add_argument('--deadline', type=to_datetime,
                                       help='the ultimate time a task can be DONE '
                                            '(quoted string in any '
                                            'commonly-used datetime format)')
    create_task_subparser.add_argument('-p', '--priority', type=to_priority,
                                       help='task significance: none, low, medium, high')
    create_task_subparser.set_defaults(handler=handlers.create_task)

    delete_task_subparser = task_subparsers.add_parser('delete', help='use to delete task')
    delete_task_subparser.add_argument('task_id', type=int,
                                       help='id of the task to be deleted')
    delete_task_subparser.set_defaults(handler=handlers.delete_task)

    update_task_subparser = task_subparsers.add_parser('update', help='use to update task')
    update_task_subparser.add_argument('task_id', type=int,
                                       help='id of the task to be updated')
    update_task_subparser.add_argument('-g', '--group-id', type=int,
                                       help='id of the new group')
    update_task_subparser.add_argument('--deadline', type=to_datetime,
                                       help='new deadline for the task')
    update_task_subparser.add_argument('-t', '--title',
                                       help='new title for the task')
    update_task_subparser.add_argument('-d', '--description',
                                       help='new description for the task')
    update_task_subparser.add_argument('-p', '--priority', type=to_priority,
                                       help='new priority')
    update_task_subparser.set_defaults(handler=handlers.update_task)

    get_task_subparser = task_subparsers.add_parser('get', help='get task by id')
    get_task_subparser.add_argument('task_id', type=int,
                                    help='is of the task wanted')
    get_task_subparser.set_defaults(handler=handlers.get_task)

    copy_task_subparser = task_subparsers.add_parser('copy', help='use to copy and shift task')
    copy_task_subparser.add_argument('task_id', type=int, help='id of the task to copy')
    copy_task_subparser.add_argument('-d', '--new-deadline', type=to_datetime,
                                     help='new deadline for a copy')
    copy_task_subparser.add_argument('--whole', action='store_true',
                                     help='use to copy whole task '
                                          '(including subtasks)')
    copy_task_subparser.set_defaults(handler=handlers.copy_task)

    complete_task_parser = task_subparsers.add_parser('complete',
                                                      help='use to complete tasks')
    complete_task_parser.add_argument('task_id', type=int, help='id of the task tp complete')
    complete_task_parser.set_defaults(handler=handlers.complete_task)

    cancel_task_parser = task_subparsers.add_parser('cancel', help='use to cancel tasks')
    cancel_task_parser.add_argument('task_id', type=int, help='id of the task to cancel')
    cancel_task_parser.set_defaults(handler=handlers.cancel_task)

    resume_task_parser = task_subparsers.add_parser('resume', help='use to resume tasks')
    resume_task_parser.add_argument('task_id', type=int, help='id of the task to resume')
    resume_task_parser.set_defaults(handler=handlers.resume_task)

    repeat_task_subparser = task_subparsers.add_parser('repeat',
                                                       help='use to set/unset repeat')
    repeat_task_subparser.set_defaults(handler=print_help_and_exit(repeat_task_subparser))
    repeat_task_subparsers = repeat_task_subparser.add_subparsers()
    set_repeat_task_subparser = repeat_task_subparsers.add_parser('set',
                                                                  help='use to set repeat')
    set_repeat_task_subparser.add_argument('task_id', type=int,
                                           help='id of task to set repeat to')
    group = set_repeat_task_subparser.add_mutually_exclusive_group()
    group.add_argument('--minutes', type=int, help='interval = minute (x) value')
    group.add_argument('--hours', type=int, help='interval = hour (x) value')
    group.add_argument('-d', '--days', type=int, help='interval = day (x) value')
    group.add_argument('-w', '--weeks', type=int, help='interval = week (x) value')
    group.add_argument('-m', '--months', type=int, help='interval = month (x) value')
    group.add_argument('-y', '--years', type=int, help='interval = year (x) value')
    group.add_argument('--week-days', action='store_true', help='on weekdays')
    set_repeat_task_subparser.set_defaults(handler=handlers.set_repeat)
    unset_repeat_task_subparser = repeat_task_subparsers.add_parser('unset',
                                                                    help='use to unset repeat')
    unset_repeat_task_subparser.add_argument('task_id', type=int,
                                             help='id of task to unset repeat from')
    unset_repeat_task_subparser.set_defaults(handler=handlers.unset_repeat)

    find_task_subparser = task_subparsers.add_parser('find', help='filter tasks')
    find_task_subparser.add_argument('--task-id', type=int, help='get task with such id')
    find_task_subparser.add_argument('-c', '--creator-id', type=int,
                                     help='get tasks created by this user')
    find_task_subparser.add_argument('-g', '--group-id', type=int,
                                     help='get tasks from the group')
    find_task_subparser.add_argument('-parent', '--parent-task-id', type=int,
                                     help='get subtasks of this task')
    find_task_subparser.add_argument('--deadline', type=to_datetime,
                                     help='get tasks with such deadline '
                                          '(common datetime format)')
    find_task_subparser.add_argument('-t', '--title',
                                     help='view tasks with such title')
    find_task_subparser.add_argument('-p', '--priority', type=to_priority,
                                     help='view tasks with such priority '
                                          '(NONE, LOW, MEDIUM, HIGH)')
    find_task_subparser.add_argument('-s', '--status', type=to_status,
                                     help='view tasks of the status '
                                          '(PENDING, CANCELED, '
                                          'FAILED, DONE)')
    find_task_subparser.add_argument('-r', '--repeat', action='store_true',
                                     help='get repeatable tasks')
    find_task_subparser.add_argument('--sort_mode', type=to_sort_mode,
                                     help='specify order of display '
                                          '(default, priority, deadline, status)')
    find_task_subparser.add_argument('--all', action='store_true',
                                     help='if you want all task (not just pending)')
    find_task_subparser.set_defaults(handler=handlers.get_tasks)


def add_group_subparsers(group_parser):
    group_subparsers = group_parser.add_subparsers()

    create_group_subparser = group_subparsers.add_parser('create', help='use to create group')
    create_group_subparser.add_argument('title', help='short string '
                                                      'describing a group')
    create_group_subparser.set_defaults(handler=handlers.create_group)

    delete_group_subparser = group_subparsers.add_parser('delete', help='use to delete group')
    delete_group_subparser.add_argument('group_id', type=int,
                                        help='id of the group to be deleted')
    delete_group_subparser.set_defaults(handler=handlers.delete_group)

    update_group_subparser = group_subparsers.add_parser('update', help='use to update group')
    update_group_subparser.add_argument('group_id', type=int,
                                        help='id of the group to be updated')
    update_group_subparser.add_argument('title', help='new title for the group')
    update_group_subparser.set_defaults(handler=handlers.update_group)

    get_group_subparser = group_subparsers.add_parser('get', help='get group by id')
    get_group_subparser.add_argument('group_id', type=int,
                                     help='id of the group wanted')
    get_group_subparser.set_defaults(handler=handlers.get_group)

    find_group_subparser = group_subparsers.add_parser('find', help='filter groups')
    find_group_subparser.add_argument('--group-id', type=int,
                                      help='get group with such id')
    find_group_subparser.add_argument('-c', '--creator-id', type=int,
                                      help='get groups created by this user')
    find_group_subparser.add_argument('-t', '--title',
                                      help='get groups with such title')
    find_group_subparser.set_defaults(handler=handlers.get_groups)


def add_subtask_subparsers(subtask_parser):
    subtask_subparsers = subtask_parser.add_subparsers()

    create_subtask_subparser = subtask_subparsers.add_parser('create',
                                                             help='use to create subtask')
    create_subtask_subparser.add_argument('parent_task_id', type=int,
                                          help='id of parent task')
    create_subtask_subparser.add_argument('title',
                                          help='short string describing subtask')
    create_subtask_subparser.add_argument('-d', '--description',
                                          help='more detailed info on the subtask')
    create_subtask_subparser.set_defaults(handler=handlers.create_subtask)

    update_subtask_subparser = subtask_subparsers.add_parser('update',
                                                             help='use to update subtask')
    update_subtask_subparser.add_argument('subtask_id', type=int,
                                          help='id of the subtask to update')
    update_subtask_subparser.add_argument('-t', '--title',
                                          help='new title for the subtask')
    update_subtask_subparser.add_argument('-d', '--description',
                                          help='new description for the subtask')
    update_subtask_subparser.set_defaults(handler=handlers.update_subtask)

    copy_subtask_subparser = subtask_subparsers.add_parser('copy',
                                                           help='use to copy and '
                                                                'shift subtask')
    copy_subtask_subparser.add_argument('subtask_id', type=int, help='id of subtask to copy')
    copy_subtask_subparser.add_argument('-p', '--new-parent-id', type=int,
                                        help='id of new parent task')
    copy_subtask_subparser.add_argument('--whole', action='store_true',
                                        help='copy the whole subtree')
    copy_subtask_subparser.set_defaults(handler=handlers.copy_subtask)

    find_subtask_subparser = subtask_subparsers.add_parser('find', help='filter subtasks')
    find_subtask_subparser.add_argument('parent_task_id', type=int,
                                        help='get subtasks of the task with such id')
    find_subtask_subparser.set_defaults(handler=handlers.get_subtasks)


def add_reminder_subparsers(reminder_parser):
    reminder_subparsers = reminder_parser.add_subparsers()

    set_reminder_subparser = reminder_subparsers.add_parser('set', help='use to set reminder')
    set_reminder_subparser.add_argument('task_id', type=int,
                                        help='id of the task to set reminder to')
    group = set_reminder_subparser.add_mutually_exclusive_group()
    group.add_argument('--at', type=to_datetime,
                       help='exact time when reminder should work')
    group.add_argument('--before', type=to_timedelta,
                       help='time interval before deadline (must be not None) '
                            'when the reminder should work')
    set_reminder_subparser.set_defaults(handler=handlers.set_reminder)

    dismiss_reminder_subparser = reminder_subparsers.add_parser('dismiss',
                                                                help='use to dismiss '
                                                                     'reminder')
    dismiss_reminder_subparser.add_argument('reminder_id', type=int,
                                            help='id of reminder to be dismissed')
    dismiss_reminder_subparser.set_defaults(handler=handlers.dismiss_reminder)

    find_remidner_subparser = reminder_subparsers.add_parser('find', help='filter reminders')
    find_remidner_subparser.add_argument('task_id', type=int,
                                         help='view reminders of the task with such id')
    find_remidner_subparser.set_defaults(handler=handlers.get_reminders)


def add_relation_subparser(relation_parser):
    relation_subparsers = relation_parser.add_subparsers()

    create_relation_subparser = relation_subparsers.add_parser('create',
                                                               help='use to create relation')
    create_relation_subparser.add_argument('from_task_id', type=int,
                                           help='id of the task where '
                                                'relation originites')
    create_relation_subparser.add_argument('to_task_id', type=int,
                                           help='id of the task where '
                                                'relation ends')
    create_relation_subparser.add_argument('relation_type', type=to_relation_type,
                                           help='type of the relation being created: '
                                                'follows, precedes, simultaneous, '
                                                'blocking, informative')
    create_relation_subparser.set_defaults(handler=handlers.relate_task)

    delete_relation_subparser = relation_subparsers.add_parser('delete',
                                                               help='use to delete relation')
    delete_relation_subparser.add_argument('relation_id', type=int,
                                           help='id of the relation to delete')
    delete_relation_subparser.set_defaults(handler=handlers.unrelate_task)

    find_relation_subparser = relation_subparsers.add_parser('find', help='filter relations')
    find_relation_subparser.add_argument('-f', '--from-task-id', type=int,
                                         help='view relations form this task')
    find_relation_subparser.add_argument('-t', '--to-task-id', type=int,
                                         help='view relations to this task')
    find_relation_subparser.add_argument('--relation-id', type=int,
                                         help='get relation with such id')
    find_relation_subparser.add_argument('-r', '--relation-type', type=to_relation_type,
                                         help='view relations of such type '
                                              '(follows, precedes, blocking, '
                                              'simultaneous, informative)')
    find_relation_subparser.set_defaults(handler=handlers.get_relations)


def add_permission_subparsers(permission_parser):
    permission_subparsers = permission_parser.add_subparsers()

    grant_subparser = permission_subparsers.add_parser('grant',
                                                       help='use to grant permissions')
    grant_subparser.add_argument('task_id', type=int,
                                 help='id of the task the permission to is granted')
    grant_subparser.add_argument('user_id', type=int,
                                 help='id of the user the permission is granted')
    grant_subparser.add_argument('-t', '--access-type', type=to_access_type,
                                 default=AccessType.VIEWER,
                                 help='type of access granted (default - viewer):'
                                      ' viewer, executor, manager')
    grant_subparser.set_defaults(handler=handlers.grant_permission)

    withdraw_subparser = permission_subparsers.add_parser('withdraw',
                                                          help='use to withdraw permissions')
    withdraw_subparser.add_argument('task_id', type=int,
                                    help='id of the task the permission to is withdrawn')
    withdraw_subparser.add_argument('user_id', type=int,
                                    help='id of the user the permission is taken from')
    withdraw_subparser.add_argument('-t', '--access-type', type=to_access_type,
                                    help='access type of the withdrawn permission: '
                                         'viewer, executor, manager')
    withdraw_subparser.set_defaults(handler=handlers.withdraw_permission)

    find_permission_subparser = permission_subparsers.add_parser('find',
                                                                 help='filter permissions')
    find_permission_subparser.add_argument('task_id', type=int,
                                           help='the id of task permissions '
                                                'to which are to be viewed')
    find_permission_subparser.add_argument('--permission-id', type=int,
                                           help='id of the permission wanted')
    find_permission_subparser.add_argument('-u', '--user-id', type=int,
                                           help='to whom permissions were granted')
    find_permission_subparser.add_argument('-t', '--access-type', type=to_access_type,
                                           help='access type of the permissions')
    find_permission_subparser.set_defaults(handler=handlers.get_permissions)


def parse():
    logging.info('{} - Starting parsing'.format(__name__))

    parser = argparse.ArgumentParser()
    parser.set_defaults(handler=print_help_and_exit(parser))

    subparsers = parser.add_subparsers()

    login_parser = subparsers.add_parser('login', help='use to login')
    login_parser.add_argument('user_id', type=int, help='id of the user to log in')
    login_parser.set_defaults(handler=handlers.login)

    who_parser = subparsers.add_parser('who', help='who is logged in')
    who_parser.set_defaults(handler=handlers.who)

    task_parser = subparsers.add_parser('task', help='use to manage (CRUD) single task')
    task_parser.set_defaults(handler=print_help_and_exit(task_parser))
    add_task_subparsers(task_parser)

    group_parser = subparsers.add_parser('group', help='use to manage (CRUD) single group')
    group_parser.set_defaults(handler=print_help_and_exit(group_parser))
    add_group_subparsers(group_parser)

    subtask_parser = subparsers.add_parser('subtask', help='use to manage (create, update) '
                                                           'single subtask')
    subtask_parser.set_defaults(handler=print_help_and_exit(subtask_parser))
    add_subtask_subparsers(subtask_parser)

    relation_parser = subparsers.add_parser('relation', help='use to manage relations')
    relation_parser.set_defaults(handler=print_help_and_exit(relation_parser))
    add_relation_subparser(relation_parser)

    reminder_parser = subparsers.add_parser('reminder', help='use to manage reminders')
    reminder_parser.set_defaults(handler=print_help_and_exit(reminder_parser))
    add_reminder_subparsers(reminder_parser)

    permission_parser = subparsers.add_parser('permission', help='use to manage '
                                                                 'permissions')
    permission_parser.set_defaults(handler=permission_parser)
    add_permission_subparsers(permission_parser)

    return parser.parse_args()
