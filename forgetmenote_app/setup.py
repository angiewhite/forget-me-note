from setuptools import setup, find_packages

setup(
    name='forgetmenote',
    version='1.0',
    packages=['forgetmenote', 'tests', 'forgetmenote.models',
              'forgetmenote.storage_services', 'forgetmenote.models_services'],
    url='https://bitbucket.org/angiewhite/forget-me-note/src/master/',
    license='GNU GPL (a lie)',
    author='angiewhite',
    author_email='angelinabelyasova@gmail.com',
    description='',
    include_package_data=True,
    test_suite='tests'
)