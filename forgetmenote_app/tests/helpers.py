import os
import shutil

def get_storage_path():
    return os.path.join(os.path.dirname(__file__), 'db')


def get_db_files_path():
    storage_path = get_storage_path()
    os.mkdir(storage_path)
    return storage_path


def drop_database():
    shutil.rmtree(get_storage_path())