import unittest
import forgetmenote.models_services as services
import forgetmenote.storage_services.storage as storage
import tests.helpers as helpers


class TestGroupServices(unittest.TestCase):

    def setUp(self):
        db_files_path = helpers.get_db_files_path()
        self.storage = storage.Storage(db_files_folder_path=db_files_path)
        self.manager = services.ServicesManager(storage=self.storage)
        self.current_user_id = 1

    def tearDown(self):
        helpers.drop_database()

    def test_create_group(self):
        group = self.manager.create_group(current_user_id=self.current_user_id,
                                          title='group')
        self.assertTrue(self.manager.get_groups(self.current_user_id))
        self.assertTrue(self.manager.get_groups(self.current_user_id,
                                                group_id=group.id)[0].title,
                        'group')
        self.assertTrue(self.manager.get_groups(self.current_user_id,
                                                group_id=group.id)[0].creator_id,
                        self.current_user_id)

    def test_delete_group(self):
        group = self.manager.create_group(current_user_id=self.current_user_id,
                                          title='group')
        self.assertTrue(self.manager.get_groups(current_user_id=self.current_user_id))

        self.manager.delete_group(current_user_id=self.current_user_id,
                                  group_id=group.id)
        self.assertFalse(self.manager.get_groups(current_user_id=self.current_user_id))

        group = self.manager.create_group(current_user_id=self.current_user_id,
                                          title='group')
        self.manager.create_task(current_user_id=self.current_user_id,
                                 group_id=group.id, title='task')
        with self.assertRaises(PermissionError):
            self.manager.delete_group(current_user_id=self.current_user_id,
                                      group_id=group.id)

    def test_get_groups(self):
        group = self.manager.create_group(current_user_id=self.current_user_id,
                                          title='group')
        self.assertTrue(group in
                        self.manager.get_groups(current_user_id=self.current_user_id))
        self.assertTrue(self.manager.get_groups(current_user_id=self.current_user_id,
                                                group_id=group.id, title=group.title))
        self.assertFalse(self.manager.get_groups(current_user_id=self.current_user_id,
                                                 group_id=group.id, title='Group'))

        self.manager.delete_group(current_user_id=self.current_user_id, group_id=group.id)
        self.assertFalse(group in self.manager.get_groups(current_user_id=self.current_user_id,
                                                          group_id=group.id))

    def test_get_group_tasks(self):
        group = self.manager.create_group(current_user_id=self.current_user_id,
                                          title='movies')
        task = self.manager.create_task(current_user_id=self.current_user_id,
                                        group_id=group.id, title='Ladybird')
        self.assertTrue(task in self.manager.get_group_tasks(current_user_id=self.current_user_id,
                                                             group_id=group.id))

    def test_update_group(self):
        group = self.manager.create_group(current_user_id=self.current_user_id,
                                          title='school')
        group = self.manager.update_group(current_user_id=self.current_user_id,
                                          group_id=group.id, title='university')
        self.assertEqual(group.title, 'university')


if __name__ == '__main__':
    unittest.main()