import unittest
import forgetmenote.models as models
import forgetmenote.models_services as services
import forgetmenote.storage_services.storage as storage
import tests.helpers as helpers


class TestPermissions(unittest.TestCase):

    def setUp(self):
        db_files_path = helpers.get_db_files_path()
        self.storage = storage.Storage(db_files_folder_path=db_files_path)
        self.manager = services.ServicesManager(storage=self.storage)
        self.current_user_id = 1

    def tearDown(self):
        helpers.drop_database()

    def _run(self):
        self.group = self.manager.create_group(current_user_id=self.current_user_id,
                                               title='group')
        self.task = self.manager.create_task(current_user_id=self.current_user_id,
                                             group_id=self.group.id, title='task')

    def test_creator_rights(self):
        self._run()
        with self.assertRaises(models.AccessDeniedError):
            self.manager.delete_group(current_user_id=2,
                                      group_id=self.group.id)
        with self.assertRaises(models.AccessDeniedError):
            self.manager.delete_task(current_user_id=2,
                                     task_id=self.task.id)
        with self.assertRaises(models.AccessDeniedError):
            self.manager.update_task_by_id(current_user_id=2,
                                           task_id=self.task.id,
                                           title='so sad')
        self.task = self.manager.update_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.task.id, title='Hey')
        self.assertEqual(self.task.title, 'Hey')

    def test_manager_access(self):
        self._run()
        self.manager.grant_permission(current_user_id=self.current_user_id,
                                      task_id=self.task.id, user_id=2,
                                      access_type=models.AccessType.MANAGER)
        self.task = self.manager.cancel_task(current_user_id=2,
                                             task_id=self.task.id)
        self.assertEqual(self.task.status, models.Status.CANCELED)

        self.manager.withdraw_permission(current_user_id=self.current_user_id,
                                         task_id=self.task.id, user_id=2,
                                         access_type=models.AccessType.MANAGER)
        with self.assertRaises(models.AccessDeniedError):
            self.manager.resume_task(current_user_id=2,
                                     task_id=self.task.id)

        self.task = self.manager.resume_task(current_user_id=self.current_user_id,
                                             task_id=self.task.id)
        self.assertEqual(self.task.status, models.Status.PENDING)

    def test_manager_grant_access(self):
        self._run()
        self.manager.grant_permission(current_user_id=self.current_user_id,
                                      task_id=self.task.id, user_id=2,
                                      access_type=models.AccessType.MANAGER)
        with self.assertRaises(models.AccessDeniedError):
            self.manager.grant_permission(current_user_id=2, task_id=self.task.id,
                                          user_id=self.current_user_id)
        self.manager.grant_permission(current_user_id=2, task_id=self.task.id,
                                      user_id=3)
        self.manager.grant_permission(current_user_id=2, task_id=self.task.id,
                                      user_id=2)
        self.assertTrue(self.manager.has_permission_of_type(task_id=self.task.id,
                                                            user_id=3,
                                                            access_type=models.AccessType.VIEWER))

        self.manager.withdraw_permission(current_user_id=2, task_id=self.task.id, user_id=3,
                                         access_type=models.AccessType.VIEWER)
        self.assertFalse(self.manager.has_permission_of_type(task_id=self.task.id, user_id=3,
                                                             access_type=models.AccessType.VIEWER))

        self.manager.withdraw_all_permissions(current_user_id=self.current_user_id,
                                              task_id=self.task.id, user_id=2)
        self.assertFalse(self.manager.is_viewer(user_id=2, task_id=self.task.id))

    def test_executor_access(self):
        self._run()
        with self.assertRaises(models.AccessDeniedError):
            self.manager.complete_task(current_user_id=2,
                                       task_id=self.task.id)

        self.manager.grant_permission(current_user_id=self.current_user_id,
                                      task_id=self.task.id, user_id=2,
                                      access_type=models.AccessType.EXECUTOR)

        self.manager.complete_task(current_user_id=2,
                                   task_id=self.task.id)
        self.task = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                task_id=self.task.id)
        self.assertEqual(self.task.status, models.Status.DONE)

    def test_viewer_access(self):
        self._run()
        self.assertFalse(self.manager.get_tasks(current_user_id=2,
                                                creator_id=1))

        self.manager.grant_permission(current_user_id=self.current_user_id,
                                      task_id=self.task.id, user_id=2)

        self.assertTrue(self.manager.get_tasks(current_user_id=2,
                                               creator_id=1))

        self.manager.withdraw_permission(current_user_id=self.current_user_id,
                                         task_id=self.task.id, user_id=2,
                                         access_type=models.AccessType.VIEWER)
        self.assertFalse(self.manager.has_permission_of_type(task_id=self.task.id,
                                                             user_id=2,
                                                             access_type=models.AccessType.VIEWER))


if __name__ == '__main__':
    unittest.main()