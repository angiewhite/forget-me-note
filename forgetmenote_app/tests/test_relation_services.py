import unittest
import forgetmenote.models as models
import forgetmenote.models_services as services
import forgetmenote.storage_services.storage as storage
import tests.helpers as helpers


class TestRelationServices(unittest.TestCase):

    def setUp(self):
        db_files_path = helpers.get_db_files_path()
        self.storage = storage.Storage(db_files_folder_path=db_files_path)
        self.manager = services.ServicesManager(storage=self.storage)
        self.current_user_id = 1

    def tearDown(self):
        helpers.drop_database()

    def _run(self):
        self.group = self.manager.create_group(current_user_id=self.current_user_id,
                                               title='group')
        self.task = self.manager.create_task(current_user_id=self.current_user_id,
                                             group_id=self.group.id, title='task')
        self.another_task = self.manager.create_task(current_user_id=self.current_user_id,
                                                     group_id=self.group.id, title='another task')

    def test_relate(self):
        self._run()
        self.manager.relate(current_user_id=self.current_user_id,
                            from_task_id=self.task.id,
                            to_task_id=self.another_task.id,
                            relation_type=models.RelationType.BLOCKING)
        self.assertTrue(self.manager.get_relations(current_user_id=self.current_user_id,
                                                   from_task_id=self.task.id,
                                                   to_task_id=self.another_task.id,
                                                   relation_type=models.RelationType.BLOCKING))

        group = self.manager.create_group(current_user_id=2, title='new group')
        another_task = self.manager.create_task(current_user_id=2, group_id=group.id,
                                                title='another one')
        with self.assertRaises(models.AccessDeniedError):
            self.manager.relate(current_user_id=self.current_user_id,
                                from_task_id=self.task.id,
                                to_task_id=another_task.id,
                                relation_type=models.RelationType.BLOCKING)
        with self.assertRaises(models.AccessDeniedError):
            self.manager.relate(current_user_id=2, from_task_id=self.task.id,
                                to_task_id=another_task.id,
                                relation_type=models.RelationType.BLOCKING)

        self.manager.grant_permission(current_user_id=self.current_user_id,
                                      task_id=self.task.id, user_id=2)
        self.manager.relate(current_user_id=2, from_task_id=self.task.id,
                            to_task_id=another_task.id,
                            relation_type=models.RelationType.BLOCKING)
        self.assertTrue(self.manager.get_relations(current_user_id=2,
                                                   from_task_id=self.task.id,
                                                   to_task_id=another_task.id,
                                                   relation_type=models.RelationType.BLOCKING))

    def test_delete_relation(self):
        self._run()
        relation = self.manager.relate(current_user_id=self.current_user_id,
                                       from_task_id=self.task.id,
                                       to_task_id=self.another_task.id,
                                       relation_type=models.RelationType.BLOCKING)
        with self.assertRaises(models.AccessDeniedError):
            self.manager.delete_relation(current_user_id=2, relation_id=relation.id)
        self.manager.delete_relation(current_user_id=self.current_user_id,
                                     relation_id=relation.id)
        self.assertFalse(self.manager.get_relations(current_user_id=self.current_user_id,
                                                    relation_id=relation.id))
        with self.assertRaises(models.ModelObjectNotFoundError):
            self.manager.delete_relation(current_user_id=self.current_user_id,
                                         relation_id=relation.id)

    def test_get_relations(self):
        self._run()
        relation = self.manager.relate(current_user_id=self.current_user_id,
                                       from_task_id=self.task.id,
                                       to_task_id=self.another_task.id,
                                       relation_type=models.RelationType.BLOCKING)
        self.assertTrue(self.manager.get_relations(current_user_id=self.current_user_id))

        self.manager.delete_task(current_user_id=self.current_user_id,
                                 task_id=self.another_task.id)
        self.assertFalse(self.manager.get_relations(current_user_id=self.current_user_id,
                                                    relation_id=relation.id))

        group = self.manager.create_group(current_user_id=2, title='new group')
        another_task = self.manager.create_task(current_user_id=2, group_id=group.id,
                                                title='new task')
        self.manager.grant_permission(current_user_id=2, task_id=another_task.id,
                                      user_id=self.current_user_id)
        relation = self.manager.relate(current_user_id=self.current_user_id,
                                       from_task_id=self.task.id,
                                       to_task_id=another_task.id,
                                       relation_type=models.RelationType.FOLLOWS)
        self.assertTrue(self.manager.get_relations(current_user_id=self.current_user_id,
                                                   relation_id=relation.id))
        self.manager.withdraw_permission(current_user_id=2, task_id=another_task.id,
                                         user_id=self.current_user_id,
                                         access_type=models.AccessType.VIEWER)
        self.assertFalse(self.manager.get_relations(current_user_id=self.current_user_id,
                                                    relation_id=relation.id))


if __name__ == '__main__':
    unittest.main()