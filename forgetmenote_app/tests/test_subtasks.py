import unittest
import datetime
import dateutil.rrule as rrule
import forgetmenote.models as models
import forgetmenote.models_services as services
import forgetmenote.storage_services.storage as storage
import tests.helpers as helpers


class TestSubtasks(unittest.TestCase):

    def setUp(self):
        db_files_path = helpers.get_db_files_path()
        self.storage = storage.Storage(db_files_folder_path=db_files_path)
        self.manager = services.ServicesManager(storage=self.storage)
        self.current_user_id = 1

    def tearDown(self):
        helpers.drop_database()

    def _run(self):
        self.group = self.manager.create_group(current_user_id=self.current_user_id,
                                               title='group')
        self.task = self.manager.create_task(current_user_id=self.current_user_id,
                                             group_id=self.group.id, title='task')
        self.subtask = self.manager.create_subtask(current_user_id=self.current_user_id,
                                                   parent_task_id=self.task.id,
                                                   title='subtask', description='dd')

    def test_create_subtask(self):
        self._run()
        self.assertIsNone(self.task.parent_task_id)
        self.assertEqual(self.subtask.parent_task_id, self.task.id)
        self.assertEqual(self.subtask.title, 'subtask')
        self.assertEqual(self.subtask.description, 'dd')
        self.assertEqual(self.subtask.deadline, self.task.deadline)
        self.assertEqual(self.subtask.priority, self.task.priority)
        self.assertEqual(self.subtask.group_id, self.task.group_id)

    def test_get_subtasks(self):
        self._run()
        self.assertTrue(self.manager.get_subtasks(current_user_id=self.current_user_id,
                                                  task_id=self.task.id))
        self.assertTrue(self.subtask in
                        self.manager.get_subtasks(current_user_id=self.current_user_id,
                                                  task_id=self.task.id))
        self.manager.delete_task(current_user_id=self.current_user_id,
                                 task_id=self.subtask.id)
        self.assertFalse(self.manager.get_subtasks(current_user_id=self.current_user_id,
                                                   task_id=self.task.id))

    def test_delete_task(self):
        self._run()
        self.manager.delete_task(current_user_id=self.current_user_id,
                                 task_id=self.task.id)
        with self.assertRaises(models.ModelObjectNotFoundError):
            self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                        task_id=self.subtask.id)

    def test_update_task(self):
        self._run()
        with self.assertRaises(PermissionError):
            self.manager.update_task_by_id(current_user_id=self.current_user_id,
                                           task_id=self.subtask.id)
        self.task = self.manager.update_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.task.id,
                                                   deadline=datetime.datetime.now())
        self.subtask = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.subtask.id)
        self.assertEqual(self.task.deadline, self.subtask.deadline)

        rule = rrule.rrule(freq=rrule.MINUTELY, interval=1, count=2,
                           dtstart=self.task.deadline)
        self.assertIsNone(self.subtask.repeat_period)
        self.task = self.manager.set_repeat(current_user_id=self.current_user_id,
                                            task_id=self.task.id, repeat_period=rule)
        self.subtask = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.subtask.id)
        self.assertIsNotNone(self.subtask.repeat_period)

    def test_complete_task(self):
        self._run()
        another_subtask = self.manager.create_subtask(current_user_id=self.current_user_id,
                                                      parent_task_id=self.task.id,
                                                      title='subtask two')
        subsubtask = self.manager.create_subtask(current_user_id=self.current_user_id,
                                                 parent_task_id=another_subtask.id,
                                                 title='subtask of subtask')
        self.manager.complete_task(current_user_id=self.current_user_id,
                                   task_id=another_subtask.id)
        another_subtask = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                      task_id=another_subtask.id)
        subsubtask = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                 task_id=subsubtask.id)
        self.assertTrue(another_subtask.status is models.Status.DONE)
        self.assertTrue(subsubtask.status is models.Status.DONE)
        self.assertFalse(self.task.status is models.Status.DONE)

        self.manager.complete_task(current_user_id=self.current_user_id,
                                   task_id=self.task.id)
        self.subtask = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.subtask.id)
        self.assertTrue(self.subtask.status is models.Status.DONE)

    def test_cancel_resume_task(self):
        self._run()
        self.task = self.manager.cancel_task(current_user_id=self.current_user_id,
                                             task_id=self.task.id)
        self.subtask = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.subtask.id)
        self.assertTrue(self.subtask.status is models.Status.CANCELED)

        self.task = self.manager.resume_task(current_user_id=self.current_user_id,
                                             task_id=self.task.id)
        self.subtask = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.subtask.id)
        self.assertTrue(self.subtask.status is models.Status.PENDING)

    def test_failed_tasks(self):
        self._run()
        fixed_time = datetime.datetime.now()
        self.task = self.manager.update_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.task.id, deadline=fixed_time)
        failed_tasks, _, __ = self.manager.get_overdue_objects(current_user_id=self.current_user_id)
        self.task = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                task_id=self.task.id)
        self.subtask = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.subtask.id)
        self.assertTrue(self.task in failed_tasks)
        self.assertTrue(self.subtask in failed_tasks)

    def test_permissions(self):
        self._run()
        self.manager.grant_permission(current_user_id=self.current_user_id,
                                      task_id=self.task.id, user_id=3,
                                      access_type=models.AccessType.VIEWER)
        self.assertTrue(self.manager.is_viewer(user_id=3, task_id=self.subtask.id))
        with self.assertRaises(models.AccessDeniedError):
            self.manager.create_subtask(current_user_id=3, parent_task_id=self.task.id,
                                        title='subtazzzk')


if __name__ == '__main__':
    unittest.main()