import unittest
import datetime
import dateutil.rrule as rrule
import forgetmenote.models as models
import forgetmenote.models_services as services
import forgetmenote.storage_services.storage as storage
import tests.helpers as helpers


class TestGeneralTaskServices(unittest.TestCase):

    def setUp(self):
        db_files_path = helpers.get_db_files_path()
        self.storage = storage.Storage(db_files_folder_path=db_files_path)
        self.manager = services.ServicesManager(storage=self.storage)
        self.current_user_id = 1

    def tearDown(self):
        helpers.drop_database()

    def _run(self):
        self.group = self.manager.create_group(current_user_id=self.current_user_id,
                                               title='653501')
        self.task = self.manager.create_task(current_user_id=self.current_user_id,
                                             group_id=self.group.id, title='lab3',
                                             description='description',
                                             deadline=datetime.datetime(year=2018,
                                                                        month=7, day=1),
                                             priority=models.Priority.HIGH)

    def test_create_task(self):
        self._run()
        self.assertEqual(self.task.group_id, self.group.id)
        self.assertEqual(self.task.title, 'lab3')
        self.assertEqual(self.task.description, 'description')
        self.assertEqual(self.task.deadline, datetime.datetime(year=2018, month=7, day=1))
        self.assertEqual(self.task.priority, models.Priority.HIGH)

    def test_create_task_in_foreign_group(self):
        foreign_group = self.manager.create_group(current_user_id=2, title='foreign')
        with self.assertRaises(models.AccessDeniedError):
            self.manager.create_task(current_user_id=self.current_user_id,
                                     group_id=foreign_group.id, title='hey')

    def test_get_tasks(self):
        self._run()
        self.assertTrue(self.task in
                        self.manager.get_tasks(current_user_id=self.current_user_id))
        tasks_with_id = self.manager.get_tasks(current_user_id=self.current_user_id,
                                               task_id=self.task.id)
        self.assertTrue(self.task in tasks_with_id and len(tasks_with_id) == 1)

    def test_delete_task(self):
        self._run()
        self.assertTrue(self.task in
                        self.manager.get_tasks(current_user_id=self.current_user_id))
        self.manager.delete_task(current_user_id=self.current_user_id,
                                 task_id=self.task.id)
        self.assertFalse(self.task in
                         self.manager.get_tasks(current_user_id=self.current_user_id))
        with self.assertRaises(models.ModelObjectNotFoundError):
            self.manager.delete_task(current_user_id=self.current_user_id,
                                     task_id=self.task.id)

    def test_update_tasks(self):
        self._run()
        fixed_time = datetime.datetime.now()
        self.assertIsNone(self.task.last_updated_time)

        self.task = self.manager.update_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.task.id,
                                                   title='New title')
        self.assertTrue(self.task.last_updated_time > fixed_time)
        self.assertEqual(self.task.title, 'New title')

        self.task.title = 'Angelina'
        self.task = self.manager.update_task(current_user_id=self.current_user_id,
                                             task=self.task)
        self.assertEqual(self.task.title, 'Angelina')
        self.manager.delete_task(current_user_id=self.current_user_id,
                                 task_id=self.task.id)
        with self.assertRaises(models.ModelObjectNotFoundError):
            self.manager.update_task_by_id(current_user_id=self.current_user_id,
                                           task_id=self.task.id,
                                           title='hi')

    def test_handle_overdue_pending_tasks(self):
        self._run()
        self.task = self.manager.update_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.task.id,
                                                   deadline=datetime.datetime.now())
        overdue_tasks, _ = self.manager.handle_overdue_pending_tasks(current_user_id=
                                                                     self.current_user_id,
                                                                     task_id=self.task.id)
        self.task = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                task_id=self.task.id)
        self.assertTrue(self.task in overdue_tasks)

    def test_handle_overdue_pending_tasks_with_no_deadline(self):
        self._run()
        self.task = self.manager.update_task_by_id(self.current_user_id, self.task.id,
                                                   set_deadline_none=True)
        overdue_tasks, _ = self.manager.handle_overdue_pending_tasks(current_user_id=
                                                                     self.current_user_id,
                                                                     task_id=self.task.id)
        self.task = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                task_id=self.task.id)
        self.assertFalse(self.task in overdue_tasks)

    def test_complete_task(self):
        self._run()
        self.manager.complete_task(current_user_id=self.current_user_id,
                                   task_id=self.task.id)
        self.assertEqual(self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                     task_id=self.task.id).status,
                         models.Status.DONE)
        with self.assertRaises(PermissionError):
            self.manager.complete_task(self.current_user_id, self.task.id)

    def test_cancel_resume_task(self):
        self._run()
        self.task.deadline = datetime.datetime.now()
        self.task = self.manager.update_task(current_user_id=self.current_user_id,
                                             task=self.task)
        with self.assertRaises(PermissionError):
            self.manager.resume_task(current_user_id=self.current_user_id,
                                     task_id=self.task.id)
        self.manager.cancel_task(current_user_id=self.current_user_id,
                                 task_id=self.task.id)
        self.assertEqual(self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                     task_id=self.task.id).status,
                         models.Status.CANCELED)
        self.manager.resume_task(current_user_id=self.current_user_id,
                                 task_id=self.task.id)
        self.assertEqual(self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                     task_id=self.task.id).status,
                         models.Status.PENDING)
        self.manager.handle_overdue_pending_tasks(current_user_id=self.current_user_id,
                                                  task_id=self.task.id)
        with self.assertRaises(PermissionError):
            self.manager.cancel_task(current_user_id=self.current_user_id,
                                     task_id=self.task.id)

    def test_copy_task_methods(self):
        self._run()
        new_task = self.manager.copy_and_shift_task(current_user_id=self.current_user_id,
                                                    task=self.task,
                                                    new_deadline=self.task.deadline)
        self.assertTrue(new_task.is_copy(self.task))
        new_task = self.manager.copy_and_shift_task(current_user_id=self.current_user_id,
                                                    task=self.task,
                                                    new_deadline=datetime.datetime.now())
        self.assertFalse(new_task.is_copy(self.task))


class TestRepeatTask(unittest.TestCase):

    def setUp(self):
        db_files_path = helpers.get_db_files_path()
        self.storage = storage.Storage(db_files_folder_path=db_files_path)
        self.manager = services.ServicesManager(storage=self.storage)
        self.current_user_id = 1

    def tearDown(self):
        helpers.drop_database()

    def _run(self):
        self.deadline = datetime.datetime.now()
        self.group = self.manager.create_group(current_user_id=self.current_user_id,
                                               title='653501')
        self.task = self.manager.create_task(current_user_id=self.current_user_id,
                                             group_id=self.group.id, title='lab3',
                                             description='description',
                                             deadline=self.deadline,
                                             priority=models.Priority.HIGH)

    def test_repeat_with_deadline(self):
        self._run()
        rule = rrule.rrule(freq=rrule.MINUTELY, interval=2, count=2)
        self.manager.set_repeat(current_user_id=self.current_user_id,
                                task_id=self.task.id, repeat_period=rule)

        new_task = self.manager.complete_task(current_user_id=self.current_user_id,
                                              task_id=self.task.id)
        self.assertIsNotNone(new_task)
        self.assertAlmostEqual(new_task.deadline,
                               (self.deadline +
                                datetime.timedelta(minutes=2)),
                               delta=datetime.timedelta(seconds=1))
        self.assertTrue(new_task.repeat)
        self.manager.unset_repeat(current_user_id=self.current_user_id,
                                  task_id=new_task.id)
        new_task = self.manager.complete_task(self.current_user_id, new_task.id)
        self.assertIsNone(new_task)

    def test_repeat_without_deadline(self):
        self._run()
        rule = rrule.rrule(freq=rrule.MINUTELY, interval=2, count=2)
        self.manager.set_repeat(current_user_id=self.current_user_id,
                                task_id=self.task.id,repeat_period=rule)
        self.task = self.manager.update_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.task.id,
                                                   set_deadline_none=True)
        self.assertTrue(self.manager.get_tasks(current_user_id=self.current_user_id,
                                               without_deadline=True))
        self.assertIsNone(self.task.repeat_period)
        new_task = self.manager.complete_task(current_user_id=self.current_user_id,
                                              task_id=self.task.id)
        self.assertIsNotNone(new_task)
        self.assertTrue(new_task.repeat)
        self.assertEqual(new_task.deadline, None)


class TestRepeatTaskWithReminders(unittest.TestCase):

    def setUp(self):
        db_files_path = helpers.get_db_files_path()
        self.storage = storage.Storage(db_files_folder_path=db_files_path)
        self.manager = services.ServicesManager(storage=self.storage)
        self.current_user_id = 1

    def tearDown(self):
        helpers.drop_database()

    def _run(self):
        self.deadline = datetime.datetime.now()
        self.group = self.manager.create_group(current_user_id=self.current_user_id,
                                               title='653501')
        self.task = self.manager.create_task(current_user_id=self.current_user_id,
                                             group_id=self.group.id, title='lab3',
                                             description='description',
                                             deadline=self.deadline,
                                             priority=models.Priority.HIGH)
        rule = rrule.rrule(freq=rrule.MINUTELY, interval=2, count=2,
                           dtstart=self.task.deadline)
        self.manager.set_repeat(current_user_id=self.current_user_id,
                                task_id=self.task.id, repeat_period=rule)
        self.manager.set_reminder(current_user_id=self.current_user_id,
                                  task_id=self.task.id,
                                  before=datetime.timedelta(minutes=1))

    def test_for_cancelled_task_with_repeat_period(self):
        self._run()
        self.manager.cancel_task(current_user_id=self.current_user_id,
                                 task_id=self.task.id)
        self.manager.handle_overdue_pending_reminders(current_user_id=self.current_user_id,
                                                      task_id=self.task.id)
        reminder = self.manager.get_reminders(current_user_id=self.current_user_id,
                                              task_id=self.task.id)[0]
        self.assertEqual(reminder.status, models.ReminderStatus.CANCELED)

        self.manager.resume_task(current_user_id=self.current_user_id,
                                 task_id=self.task.id)
        reminder = self.manager.get_reminders(current_user_id=self.current_user_id,
                                              task_id=self.task.id)[0]
        self.assertEqual(reminder.status, models.ReminderStatus.WORKED)

        failed_tasks, new_tasks = \
            self.manager.handle_overdue_pending_tasks(current_user_id= self.current_user_id,
                                                      task_id=self.task.id)
        self.task = self.manager.get_task_by_id(current_user_id=self.current_user_id,
                                                task_id=self.task.id)
        self.assertTrue(self.task in failed_tasks)
        self.assertTrue(new_tasks)

        new_reminders = self.manager.get_reminders(current_user_id=self.current_user_id,
                                                   task_id=new_tasks[0].id)
        self.assertTrue(new_reminders)
        self.assertAlmostEqual(new_reminders[0].time, reminder.time
                               + datetime.timedelta(minutes=2),
                               delta=datetime.timedelta(seconds=1))
        self.manager.dismiss_reminder(current_user_id=self.current_user_id,
                                      reminder_id=new_reminders[0].id)
        self.assertFalse(self.manager.get_reminders(current_user_id=self.current_user_id,
                                                    task_id=new_tasks[0].id))
        with self.assertRaises(models.ModelObjectNotFoundError):
            self.manager.dismiss_reminder(current_user_id=self.current_user_id,
                                          reminder_id=new_reminders[0].id)

    def test_for_tasks_with_repeat_and_reminders_no_deadline(self):
        self._run()
        self.task = self.manager.update_task_by_id(current_user_id=self.current_user_id,
                                                   task_id=self.task.id,
                                                   set_deadline_none=True)
        new_task = self.manager.complete_task(current_user_id=self.current_user_id,
                                              task_id=self.task.id)
        new_task_reminders = self.manager.get_reminders(current_user_id=self.current_user_id,
                                                        task_id=new_task.id)
        self.assertFalse(new_task_reminders)


if __name__ == '__main__':
    unittest.main()