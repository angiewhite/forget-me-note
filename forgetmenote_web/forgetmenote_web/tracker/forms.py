from django import forms
from django.utils.translation import gettext_lazy

from forgetmenote import Priority, AccessType

from . import models


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class TaskForm(forms.ModelForm):

    class Meta:
        model = models.Task
        fields = ['group', 'title', 'description', 'deadline', 'priority']
        widgets = {
            'deadline': forms.DateTimeInput(format='%Y-%m-%d %H:%M'),
            'description': forms.Textarea(),
            'priority': forms.Select(choices=[(item.value, item.name) for item in Priority])
        }
        help_texts = {
            'deadline': gettext_lazy('format - Y-m-d H:M'),
        }


class PermissionForm(forms.ModelForm):

    class Meta:
        model = models.Permission
        fields = ['user', 'access_type']
        widgets = {
            'access_type': forms.Select(choices=[(item.value, item.name) for item in AccessType])
        }