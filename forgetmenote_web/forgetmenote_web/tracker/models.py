import dateutil.parser
from dateutil import rrule

from django.db import models
from django.contrib.auth.models import User

import forgetmenote


def nullable_int(value):
    return int(value) if value != 'None' else None


class EnumTypeField(models.Field):

    def __init__(self, enum_type, *args, **kwargs):
        self.enum = enum_type
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        return name, path, (self.enum, *args), kwargs

    def get_prep_value(self, value):
        return value.value

    def to_python(self, value):
        if isinstance(value, self.enum):
            return value

        if isinstance(value, str):
            value = int(value)

        if value is None:
            return value

        return self.enum(value)

    def from_db_value(self, value, expression, connection, context):
        return self.to_python(value)

    def get_internal_type(self):
        return 'IntegerField'


class RuleField(models.Field):

    def get_prep_value(self, value):
        if not value:
            return 'None'

        return '{} {} {} {} {}'.format(
            value._freq, value._interval, value._count,
            bool(value._byweekday), str(value._dtstart)
        )

    def to_python(self, value):
        if isinstance(value, rrule.rrule):
            return value

        if value is None:
            return value

        if value == 'None':
            return None

        args = value.split()

        byweekday = [rrule.MO, rrule.TU, rrule.WE,
                     rrule.TH, rrule.FR]
        if args[3] == 'False':
            byweekday = []

        rule = rrule.rrule(
            freq=nullable_int(args[0]), count=nullable_int(args[2]),
            byweekday=byweekday, interval=int(args[1]),
            dtstart=dateutil.parser.parse(args[4])
        )
        return rule

    def from_db_value(self, value, expression, connection, context):
        return self.to_python(value)

    def get_internal_type(self):
        return 'CharField'


class Group(models.Model):

    class ExplicitGroupManager(models.Manager):
        def create_object(self, creator_id, title):
            return self.create(creator_id=creator_id, title=title)

    creator = models.ForeignKey(User, on_delete=models.PROTECT,
                                related_name='created_groups')
    title = models.CharField(max_length=30)

    objects = ExplicitGroupManager()

    def __str__(self):
        return self.title


class Task(models.Model):

    class ExplicitTaskManager(models.Manager):

        def create_object(self, creator_id, parent_task_id, group_id,
                          title, description, deadline, priority, status,
                          creation_time, last_updated_time):
            return self.create(creator_id=creator_id, parent_task_id=parent_task_id,
                               group_id=group_id, title=title,
                               description=description, deadline=deadline,
                               priority=priority, status=status,
                               creation_time=creation_time,
                               last_updated_time=last_updated_time)

    creator = models.ForeignKey(User, on_delete=models.PROTECT,
                                related_name='created_tasks')
    parent_task = models.ForeignKey('Task', related_name='child_tasks',
                                    null=True, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.PROTECT,
                              related_name='tasks')
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=500, blank=True, null=True)
    deadline = models.DateTimeField(blank=True, null=True)
    creation_time = models.DateTimeField(editable=False)
    last_updated_time = models.DateTimeField(blank=True, null=True)
    priority = EnumTypeField(forgetmenote.Priority)
    status = EnumTypeField(forgetmenote.Status)
    repeat = models.BooleanField(blank=True, default=False)
    repeat_period = RuleField(max_length=100, null=True)
    related_tasks = models.ManyToManyField('Task', through='Relation',
                                           symmetrical=False)

    objects = ExplicitTaskManager()


class Reminder(models.Model):

    class ExplicitReminderManager(models.Manager):
        def create_object(self, task_id, time, status):
            return self.create(task_id=task_id, time=time, status=status)

    task = models.ForeignKey(Task, on_delete=models.CASCADE,
                             related_name='reminders')
    time = models.DateTimeField()
    status = EnumTypeField(forgetmenote.ReminderStatus)

    objects = ExplicitReminderManager()


class Permission(models.Model):
    class ExplicitPermissionManager(models.Manager):
        def create_object(self, task_id, user_id, access_type):
            return self.create(task_id=task_id, user_id=user_id,
                               access_type=access_type)

    task = models.ForeignKey(Task, on_delete=models.CASCADE,
                             related_name='permissions')
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='available_permissions')
    access_type = EnumTypeField(forgetmenote.AccessType)

    objects = ExplicitPermissionManager()


class Relation(models.Model):
    class ExplicitRelationManager(models.Manager):
        def create_object(self, creator_id, from_task_id, to_task_id, relation_type):
            return self.create(creator_id=creator_id, from_task_id=from_task_id,
                               to_task_id=to_task_id, relation_type=relation_type)

    from_task = models.ForeignKey(Task, on_delete=models.CASCADE,
                                  related_name='related_to')
    to_task = models.ForeignKey(Task, on_delete=models.CASCADE,
                                related_name='related_from')
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    relation_type = EnumTypeField(forgetmenote.RelationType)

    objects = ExplicitRelationManager()
