import forgetmenote
import forgetmenote.str_constants as constants
from . import models


def task_converter(task_model_object):
    priority = task_model_object.priority
    status = task_model_object.status
    task = forgetmenote.Task(task_model_object.id, task_model_object.creator_id,
                             task_model_object.parent_task_id, task_model_object.group_id,
                             task_model_object.title, task_model_object.description,
                             task_model_object.deadline, forgetmenote.Priority(priority),
                             forgetmenote.Status(status), task_model_object.creation_time,
                             task_model_object.last_updated_time)
    task.repeat = task_model_object.repeat
    if task_model_object.repeat_period:
        task.repeat_period = task_model_object.repeat_period
    return task


def group_converter(group_model_object):
    return forgetmenote.Group(group_model_object.id, group_model_object.creator_id,
                              group_model_object.title)


def relation_converter(relation_model_object):
    relation_type = relation_model_object.relation_type
    return forgetmenote.Relation(relation_model_object.id,
                                 relation_model_object.creator_id,
                                 relation_model_object.from_task_id,
                                 relation_model_object.to_task_id,
                                 forgetmenote.RelationType(relation_type))


def reminder_converter(reminder_model_object):
    reminder_status = reminder_model_object.status
    return forgetmenote.Reminder(reminder_model_object.id,
                                 reminder_model_object.task_id,
                                 reminder_model_object.time,
                                 forgetmenote.ReminderStatus(reminder_status))


def permission_converter(permission_model_object):
    access_type = permission_model_object.access_type
    return forgetmenote.Permission(permission_model_object.id,
                                   permission_model_object.task_id,
                                   permission_model_object.user_id,
                                   forgetmenote.AccessType(access_type))


FROM_MODELS_CONVERTERS = {models.Task: task_converter,
                          models.Group: group_converter,
                          models.Reminder: reminder_converter,
                          models.Relation: relation_converter,
                          models.Permission: permission_converter}


LIBRARY_TYPE_TO_MODEL_MAPPING = {forgetmenote.Task: models.Task,
                                 forgetmenote.Group: models.Group,
                                 forgetmenote.Reminder: models.Reminder,
                                 forgetmenote.Relation: models.Relation,
                                 forgetmenote.Permission: models.Permission}


class Storage:

    def create(self, request_type, *args, **kwargs):
        model_type = LIBRARY_TYPE_TO_MODEL_MAPPING[request_type]
        created_object = model_type.objects.create_object(*args, **kwargs)
        return request_type(created_object.id, *args, **kwargs)

    def delete(self, request_type, obj_id):
        model_type = LIBRARY_TYPE_TO_MODEL_MAPPING[request_type]
        model_type.objects.get(id=obj_id).delete()

    def update(self, request_type, obj_id, **kwargs):
        model_type = LIBRARY_TYPE_TO_MODEL_MAPPING[request_type]
        model_type.objects.filter(id=obj_id).update(**kwargs)
        updated_object = model_type.objects.get(id=obj_id)
        return FROM_MODELS_CONVERTERS[model_type](updated_object)

    def get(self, request_type, **kwargs):
        model_type = LIBRARY_TYPE_TO_MODEL_MAPPING[request_type]
        filtered_objects = model_type.objects.filter(**kwargs)
        return [FROM_MODELS_CONVERTERS[model_type](obj) for obj in filtered_objects]

    def get_or_raise_error(self, request_type, **kwargs):
        result = self.get(request_type, **kwargs)

        if result:
            return result

        look_up_id = kwargs.get(constants.OBJ_ID, '?')
        raise forgetmenote.ModelObjectNotFoundError(request_type, look_up_id)