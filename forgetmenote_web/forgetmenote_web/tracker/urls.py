from django.conf.urls import url

from . import views
urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^logout/$', views.logout_user, name='logout'),
    url(r'^signup/$', views.sign_up, name='sign_up'),
    url(r'^task/create/$', views.create_task, name='task_create'),
    url(r'^task/$', views.list_tasks, name='list_tasks'),
    url(r'^task/view/$', views.view_task, name='task_view'),
    url(r'^task/edit/$', views.edit_task, name='task_edit'),
    url(r'^task/delete/$', views.delete_task, name='task_delete'),
    url(r'^task/complete/$', views.complete_task, name='task_complete'),
    url(r'^task/cancel/$', views.cancel_task, name='task_cancel'),
    url(r'^task/resume/$', views.resume_task, name='task_resume'),
    url(r'^task/copy/$', views.copy_task, name='task_copy'),
    url(r'^group/create/$', views.create_group, name='group_create'),
    url(r'^group/delete/$', views.delete_group, name='group_delete'),
    url(r'^group/view/$', views.view_group, name='group_view'),
    url(r'^group/edit/$', views.edit_group, name='group_edit'),
    url(r'^reminder/set/$', views.set_reminder, name='reminder_set'),
    url(r'^reminder/dismiss/$', views.dismiss_reminder, name='reminder_dismiss'),
    url(r'^permission/withdraw/$', views.withdraw_permission, name='permission_withdraw'),
    url(r'^permission/grant/$', views.grant_permission, name='permission_grant'),
    url(r'^repeat/set/$', views.set_repeat, name='repeat_set'),
    url(r'^repeat/unset/$', views.unset_repeat, name='repeat_unset'),
]

