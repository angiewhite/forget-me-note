import dateutil.parser
from dateutil import relativedelta
from dateutil import rrule

from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm

from forgetmenote import (
    ServicesManager, Priority, Status, AccessType, AccessDeniedError
)

from . import forms
from . import models
from . import storage


def repeat_to_str(repeat):
    if not repeat:
        return 'None'

    if repeat._byweekday:
        return "Every weekday"

    FREQUENCY = {0: 'year',
                 1: 'month',
                 2: 'week',
                 3: 'day',
                 4: 'hour',
                 5: 'minute'}

    return "Every {} {}s".format(repeat._interval, FREQUENCY[repeat._freq])


def get_services_manager():
    return ServicesManager(storage.Storage())


@login_required
def index(request):
    manager = get_services_manager()
    groups = manager.get_groups(request.user.id)
    for group  in groups:
        group.tasks_number = len(manager.get_group_tasks(request.user.id, group.id))
    return render(request, 'tracker/index.html', {'groups': groups, 'user': request.user})


@login_required
def create_task(request):
    if request.method == 'GET':
        form = forms.TaskForm()
        form.fields['group'].queryset = models.Group.objects.filter(creator=request.user)
        return render(request, 'tracker/task_editable.html', {'form': form, 'action': 'Create'})

    form = forms.TaskForm(request.POST)
    form.fields['group'].queryset = models.Group.objects.filter(creator=request.user)
    if not form.is_valid():
        return render(request, 'tracker/task_editable.html', {'form': form, 'action': 'Create'})

    data = form.cleaned_data
    manager = get_services_manager()
    task = manager.create_task(
        request.user.id, data['group'].id,
        data['title'], data['description'],
        data['deadline'], Priority(int(data['priority']))
    )
    return redirect(reverse('task_view') + '?task_id={}'.format(task.id))


@login_required
def edit_task(request):
    task_id = request.GET.get('task_id')
    if request.method == 'GET':
        task = models.Task.objects.get(id=task_id)
        fields = {
            'title': task.title,
            'description': task.description,
            'priority': task.priority.value,
            'deadline': task.deadline,
            'group': task.group.id,
        }
        form = forms.TaskForm(fields)
        form.fields['group'].queryset = models.Group.objects.filter(creator=request.user)
        return render(request, 'tracker/task_editable.html', {'form': form, 'action': 'Edit', 'task_id': task_id})

    form = forms.TaskForm(request.POST)
    form.fields['group'].queryset = models.Group.objects.filter(creator=request.user)
    if not form.is_valid():
        return render(request, 'tracker/task_editable.html', {'form': form, 'action': 'Edit', 'task_id': task_id})

    data = form.cleaned_data
    task = models.Task.objects.get(id=task_id)
    manager = get_services_manager()
    task.title = data['title']
    task.description = data['description'] or ''
    task.deadline = data['deadline']
    task.group = data['group']
    task.priority = Priority(int(data['priority']))
    manager.update_task(request.user.id, task)
    manager.get_task_by_id(request.user.id, task.id)
    return redirect(reverse('task_view') + '?task_id={}'.format(task.id))


@login_required
def copy_task(request):
    task_id = request.GET.get('task_id')
    if request.method == 'GET':
        task = models.Task.objects.get(id=task_id)
        fields = {
            'title': task.title,
            'description': task.description,
            'priority': task.priority.value,
            'deadline': task.deadline,
            'group': task.group.id,
        }
        form = forms.TaskForm(fields)
        return render(request, 'tracker/task_editable.html', {'form': form, 'action': 'Copy', 'task_id': task_id})

    form = forms.TaskForm(request.POST)
    form.fields['group'].queryset = models.Group.objects.filter(creator=request.user)
    if not form.is_valid():
        return render(request, 'tracker/task_editable.html', {'form': form, 'action': 'Copy', 'task_id': task_id})

    data = form.cleaned_data
    manager = get_services_manager()
    task = models.Task.objects.get(id=task_id)
    new_task = manager.copy_and_shift_task(
        request.user.id, task, data['deadline']
    )
    return redirect(reverse('task_view') + '?task_id={}'.format(new_task.id))


@login_required
def complete_task(request):
    manager = get_services_manager()
    task_id = request.POST.get('task_id')
    manager.complete_task(request.user.id, task_id)
    task = models.Task.objects.get(id=task_id)
    return JsonResponse({'status': task.status.name})


@login_required
def cancel_task(request):
    manager = get_services_manager()
    task_id = request.POST.get('task_id')
    manager.cancel_task(request.user.id, task_id)
    task = models.Task.objects.get(id=task_id)
    return JsonResponse({'status': task.status.name})


@login_required
def resume_task(request):
    manager = get_services_manager()
    task_id = request.POST.get('task_id')
    manager.resume_task(request.user.id, task_id)
    task = models.Task.objects.get(id=task_id)
    return JsonResponse({'status': task.status.name})


@login_required
def delete_task(request):
    manager = get_services_manager()
    manager.delete_task(request.user.id, request.POST.get('task_id'))
    return redirect(reverse('list_tasks'))


@login_required
def list_tasks(request):
    if request.method == 'GET':
        manager = get_services_manager()
        tasks = manager.get_tasks(request.user.id, parent_task_id=None)
        return render(
            request, 'tracker/task_list.html',
            {'tasks': tasks, 'status': (
                ('pending', Status.PENDING),
                ('failed', Status.FAILED),
                ('done', Status.DONE),
                ('canceled', Status.CANCELED)
            )
            }
        )


@login_required
def view_task(request):
    task_id = request.GET.get('task_id')
    user_id = request.user.id
    manager = get_services_manager()
    if not manager.is_viewer(user_id, task_id):
        raise Http404
    task = models.Task.objects.get(id=task_id)
    is_creator = manager.is_creator(user_id, task_id)
    is_manager = manager.is_manager(user_id, task_id)
    is_executor = manager.is_executor(user_id, task_id)
    can_edit = is_creator and task.status is Status.PENDING
    managers = models.Permission.objects.filter(task=task, access_type=AccessType.MANAGER)
    executors = models.Permission.objects.filter(task=task, access_type=AccessType.EXECUTOR)
    viewers = models.Permission.objects.filter(task=task, access_type=AccessType.VIEWER)
    reminders = manager.get_reminders(request.user.id, task.id)

    users = models.User.objects.all()
    return render(request, 'tracker/task_view.html', {
        'task': task, 'can_edit': can_edit, 'is_creator': is_creator,
        'managers': managers, 'executors': executors, 'viewers': viewers,
        'reminders': reminders, 'is_manager': is_manager, 'is_executor': is_executor,
        'repeat_period': repeat_to_str(task.repeat_period)
    })


@login_required
def set_repeat(request):
    task = models.Task.objects.get(id=request.POST.get('task_id'))

    repeat_period = None

    if task.deadline:
        freq = rrule.DAILY
        interval = 1
        byweekday = None

        frequency = request.POST.get('frequency')
        if frequency == 'byweekday':
            byweekday = [rrule.MO, rrule.TU, rrule.WE,
                         rrule.TH, rrule.FR]
        else:
            freq = rrule.__getattribute__(frequency)
            interval = int(request.POST.get('interval') or 1)

        repeat_period = rrule.rrule(
            freq=freq, interval=interval, byweekday=byweekday,
            count=2, dtstart=task.deadline
        )

    manager = get_services_manager()
    task = manager.set_repeat(request.user.id, task.id, repeat_period)

    return JsonResponse({'repeat_period': repeat_to_str(task.repeat_period)})


@login_required
def unset_repeat(request):
    manager = get_services_manager()
    manager.unset_repeat(request.user.id, request.POST.get('task_id'))
    return HttpResponse(status=201)


@login_required
def create_group(request):
    manager = get_services_manager()
    group = manager.create_group(request.user.id, request.POST.get('title'))
    return JsonResponse({'group_id': group.id})


@login_required
def delete_group(request):
    manager = get_services_manager()
    try:
        manager.delete_group(request.user.id, request.POST.get('group_id'))
    except PermissionError:
        return HttpResponse('Group is not empty!', status=403)
    return HttpResponse(status=201)


@login_required
def view_group(request):
    manager = get_services_manager()
    group = manager.get_group_by_id(request.user.id, request.GET.get('group_id'))
    group.creator = models.Group.objects.get(id=group.id).creator
    tasks = manager.get_group_tasks(request.user.id, group.id)
    return render(request, 'tracker/group_view.html', {'group': group, 'tasks': tasks})


@login_required
def edit_group(request):
    manager = get_services_manager()
    group = manager.update_group(
        request.user.id, request.POST.get('group_id'), request.POST.get('title')
    )
    return JsonResponse({'title': group.title})


@login_required
def set_reminder(request):
    manager = get_services_manager()
    at = request.POST.get('at')
    before = None
    if at:
        at = dateutil.parser.parse(at)
    else:
        before = relativedelta.relativedelta(**{
            request.POST.get('period'): int(request.POST.get('count'))
        })
    reminder = manager.set_reminder(
        request.user.id, request.POST.get('task_id'),
        at=at, before=before
    )
    return JsonResponse({'reminder_id': reminder.id, 'time': reminder.time.strftime('%b %d, %Y, %I:%M  %p')})


@login_required
def dismiss_reminder(request):
    manager = get_services_manager()
    manager.dismiss_reminder(request.user.id, request.POST.get('reminder_id'))
    return HttpResponse(status=201)


@login_required
def grant_permission(request):
    manager = get_services_manager()
    if request.method == 'GET':
        task = models.Task.objects.get(id=request.GET.get('task_id'))
        if not manager.is_manager(request.user.id, task.id):
            raise Http404
        form = forms.PermissionForm()
        form.fields['user'].queryset = models.User.objects.exclude(id=task.creator.id)
        return render(request, 'tracker/permission_grant.html', {'form': form, 'task_id': request.GET.get('task_id')})

    form = forms.PermissionForm(request.POST)
    form.fields['user'].queryset = models.User.objects.exclude(id=request.user.id)
    if not form.is_valid():
        return render(request, 'tracker/permission_grant.html', {'form': form})

    data = form.cleaned_data
    try:
        manager.grant_permission(
            request.user.id, request.GET.get('task_id'),
            data['user'].id, AccessType(int(data['access_type']))
        )
    except AccessDeniedError as e:
        error = e.args[0]
        return render(request, 'tracker/permission_grant.html', {'form': form, 'error': error})
    return redirect(reverse('task_view') + '?task_id={}'.format(request.GET.get('task_id')))


@login_required
def withdraw_permission(request):
    manager = get_services_manager()
    permission = models.Permission.objects.get(id=request.POST.get('permission_id'))
    manager.withdraw_permission(request.user.id, permission.task.id, permission.user.id, permission.access_type)
    return HttpResponse(status=201)


def login_user(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            return redirect(reverse('home'))
        form = forms.LoginForm()
        return render(request, 'tracker/login.html', {'form': form})

    form = forms.LoginForm(request.POST)
    if not form.is_valid():
        return render(request, 'tracker/login.html', {'form': form})

    data = form.cleaned_data
    username = data['username']
    password = data['password']
    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect(request.GET.get('next', reverse('home')))
        else:
            error = 'disabled account'
    else:
        form = forms.LoginForm(request.POST)
        error = 'invalid login'
    return render(request, 'tracker/login.html', {'form': form, 'error': error})


def logout_user(request):
    logout(request)
    return redirect('/tracker/')


def sign_up(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'tracker/sign_up.html', {'form': form})