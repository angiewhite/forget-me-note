from setuptools import setup, find_packages

setup(
    name='forget_me_note',
    version='1.0',
    packages=find_packages(),
    url='https://bitbucket.org/angiewhite/forget-me-note/src/master/',
    license='GNU GPL (a lie)',
    author='angiewhite',
    author_email='angelinabelyasova@gmail.com',
    description='',
    include_package_data=True,
    entry_points={
        'console_scripts': 'forgetmenote=forgetmenote_app.forgetmenote_client.main:main'
    },
    test_suite='forgetmenote_app.tests'
)
